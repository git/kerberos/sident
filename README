                                 S/Ident
                        Secure/Ident - Version 3.6

             Written by Booker C. Bense <bbense@stanford.edu>
         Currently maintained by Russ Allbery <rra@stanford.edu>

  Copyright 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005,
  2006 Board of Trustees, Leland Stanford Jr. University.  This software
  is distributed under a BSD-style license.  Please see the section
  LICENSE below for terms of use and redistribution.

  Portions based on source from Peter Eriksson <pen@lysator.liu.se>
  contained in the libident library, released into the public domain.

  Portions based on code copyright (c) 1994-2000 Carnegie Mellon
  University.  This product includes software developed by Computing
  Services at Carnegie Mellon University (http://www.cmu.edu/computing/).

SECURITY WARNING

  The S/Ident protocol implemented by this package is inherently
  vulnerable to an active man-in-the-middle attack.  If an attacker can
  interpose themselves into a network connection initiated by a victim and
  both impersonate that victim and selectively control which of their
  packets reach a server using S/Ident, the attacker can make use of the
  victim's authentication credentials.  The attacker cannot initiate the
  session, only hijack an existing authenticated session.

  Because of this, you should very carefully analyze the security
  requirements of any service for which you're considering deploying
  S/Ident authentication.  Due to the requirements of the attack, S/Ident
  may still be appropriate for very light authentication or in secure
  network environments, but should not be used for general authentication
  on untrusted networks.

DESCRIPTION

  This is a program that implements the S/Ident protocol proposed by
  Robert Morgan <morgan@stanford.edu>.  It is based on the RFC1413
  identification protocol but attempts to conduct the identification
  transaction in a secure manner.

  The code is based on the pidentd/libident code from Peter Eriksson
  <pen@lysator.liu.se> with the SASL-like kerberos exchange based on
  code from CMU's imapd-1.4 release.

  Currently, the code only supports Kerberos v4 and Kerberos v5 as
  security methods.

  For the specification of the protocol that this implementation attempts
  to follow, see doc/draft-morgan-ident-ext-04.txt.

REQUIREMENTS

  S/Ident uses Kerberos (either Kerberos v4 or GSSAPI with Kerberos v5)
  for authentication, and therefore requires Kerberos and GSSAPI libraries
  to compile.  It has primarily been tested with MIT Kerberos, but should
  also work with Heimdal.  The Kerberos v4 code has been tested with stock
  Kerberos v4 libraries and with the Kerberos v4 compatibility libraries
  included with MIT Kerberos v5, but it should also work with the KTH
  Kerberos libraries.  S/Ident can be built without Kerberos v4 support if
  you only have Kerberos v5 GSSAPI libraries available.

  The S/Ident responder has to grovel through the kernel in order to
  determine which user corresponds to a given network connection, and is
  therefore extremely sensitive to changes in the operating system
  kernel.  Among other things, this means that it will likely need to be
  recompiled with each new release of an operating system, and will need
  to be built by a 64-bit compiler to run on 64-bit Solaris.

  The platform has to be among the platforms for which there are available
  pident kernel modules.  It has been tested and verified working on:

      AIX 4.3
      IRIX 6.5
      Linux 2.x
      Digital Unix / Tru64 4.0f
      Solaris 2.6, 7, 8, and 9 (32-bit)
      Solaris 8 and 9 (64-bit)

  The kernel modules are available for all of:

      AIX 4.1, 4.2, 4.3, and 5.x
      IRIX 4.x, 5.x, and 6.x
      Linux 2.x
      NeXTSTEP 3
      Digital Unix / Tru64 4.x and 5.x
      Solaris 2.5, 2.6, 7, 8, 9, and 10

  but the other platforms have not been tested in some time.  The compiler
  used for testing was GCC in all cases except for the 64-bit Solaris
  builds, where Forte 6 was used.

  Note that this list does not contain HP-UX, nor has this code been
  tested on any BSD system, although there is a generic BSD kernel module
  available.

LICENSE

  Copyright 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005,
  2006 Board of Trustees, Leland Stanford Jr. University.

  Portions based on source from Peter Eriksson <pen@lysator.liu.se>
  contained in the libident library, released into the public domain.

  Portions based on code copyright (c) 1994-2000 Carnegie Mellon
  University.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
 
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 
  3. The name "Carnegie Mellon University" must not be used to endorse or
     promote products derived from this software without prior written
     permission.  For permission or any legal details, please contact

        Office of Technology Transfer 
        Carnegie Mellon University 
        5000 Forbes Avenue 
        Pittsburgh, PA 15213-3890 
        (412) 268-4387, fax: (412) 268-7395 
        tech-transfer@andrew.cmu.edu 
 
  4. Redistributions of any form whatsoever must retain the following 
     acknowledgment:

     "This product includes software developed by Computing Services 
     at Carnegie Mellon University (http://www.cmu.edu/computing/)." 
 
  STANFORD UNIVERSITY AND CARNEGIE MELLON UNIVERSITY DISCLAIM ALL
  WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL STANFORD
  UNIVERSITY OR CARNEGIE MELLON UNIVERSITY BE LIABLE FOR ANY SPECIAL,
  INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
  FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
  NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
  WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
