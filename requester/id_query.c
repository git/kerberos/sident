/*  $Id: id_query.c 550 2004-06-17 01:27:41Z eagle $
**
**  Low-level call to transmit a query to an ident server.
**
**  Written by Peter Eriksson <pen@lysator.liu.se>
*/

#include "config.h"

#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef HAVE_SYS_SELECT_H
# include <sys/select.h>
#endif

#include "sident.h"


/*
**  Sends off a query to a remote ident server.  resp_port and req_port are
**  sent to the server to identify the connection for which identification is
**  needed.  timeout is given as for id_open.  auth_data specifies the
**  authentication protocol to use.
**
**  Returns the number of bytes sent to the remote server on success, -1 onq
**  failure.  On failure, errno is set.
*/
int
id_query(ident_t *id, int resp_port, int req_port, struct timeval *timeout,
         struct ident_auth_client_data *auth_data)
{
    RETSIGTYPE (*old_sig)(int);
    int res;
    char buf[IDENT_BUFFER_SIZE];
    fd_set ws;

    if (auth_data != NULL) {
        char *start_data;

        auth_data->requester_port = req_port;
        auth_data->responder_port = resp_port;
        auth_data->fd = id->fd;
        auth_data->timeout = timeout;
        start_data = (*auth_data->auth_struct->start)(auth_data);
        if (start_data == NULL) {
            errno = EINVAL;     /* Arbitrary */
            return -1;
        }
        sprintf(buf, "%d , %d : AUTHENTICATE : %s\r\n", resp_port, req_port,
                start_data);
    } else
        sprintf(buf, "%d , %d\r\n", resp_port, req_port);

#ifdef DEBUG
    printf("Sending.. \n%s",buf) ;
#endif

    if (timeout) {
        FD_ZERO(&ws);
        FD_SET(id->fd, &ws);
        res = select(FD_SETSIZE, NULL, &ws, NULL, timeout);
        if (res < 0)
            return -1;
        else if (res == 0) {
            errno = ETIMEDOUT;
            return -1;
        }
    }
    old_sig = signal(SIGPIPE, SIG_IGN);
    res = write(id->fd, buf, strlen(buf));
    signal(SIGPIPE, old_sig);

    return res;
}
