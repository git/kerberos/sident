/*  $Id: id_close.c 550 2004-06-17 01:27:41Z eagle $
**
**  Low-level call to close a connection to an ident server.
**
**  Written by Peter Eriksson <pen@lysator.liu.se>
*/

#include <stdlib.h>
#include <unistd.h>

#include "sident.h"

int
id_close(ident_t *id)
{
    int res;

    res = close(id->fd);
    free(id);

    return res;
}
