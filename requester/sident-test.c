/*  $Id: sident-test.c 2430 2006-02-08 21:32:45Z rra $
**
**  This is a daemon suitable for running from tcpserver or inetd that runs
**  through a sequence of ident calls to the remote host, trying Kerberos v4,
**  Kerberos v5, and the interactive flag.  It does not test regular ident at
**  this time.
**
**  Written by P�r Emanuelsson <pell@lysator.liu.se>
**  Kerberos v4 testing added by Booker Bense <bbense@stanford.edu>
**  Kerberos v5 testing added by Russ Allbery <rra@stanford.edu>
*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "config.h"
#include "sident.h"

/*
**  Print the results of an ident query.  tag is a tag to print before the
**  output, indicating what was checked.  IDENT is the result of a call to
**  ident_lookup.
*/
static void
print_results(const char *tag, IDENT *ident)
{
    int result;
    char expiration[20];
    struct tm *tm;

    fflush(stdout);
    if (!ident) {
        perror("ident");
        return;
    }

    if (ident->expires == (time_t) -1)
        strcpy(expiration, "<never>");
    else {
        tm = localtime(&ident->expires);
        if (tm == NULL)
            strcpy(expiration, "<unknown>");
        else
            strftime(expiration, sizeof(expiration), "%Y-%m-%d %H:%M:%S", tm);
    }

    printf("S/Ident results for %s:\r\n", tag);
    printf("        Identifier:  %s\r\n",
           (ident->identifier != NULL) ? ident->identifier : "");
    printf("   Local Principal:  %s\r\n",
           (ident->principal != NULL) ? ident->principal : "");
    printf("        Expiration:  %s\r\n", expiration);
    printf("    Responder Port:  %d\r\n", ident->resp_port);
    printf("    Requester Port:  %d\r\n", ident->req_port);
    printf("  Operating System:  %s\r\n",
           (ident->opsys != NULL) ? ident->opsys : "<not specified>");
    printf("     Character Set:  %s\r\n",
           (ident->charset != NULL) ? ident->charset : "<not specified>");
    printf("       Result Code:  %d, %s\r\n", ident->result_code,
           ident_err_txt[ident->result_code]);
    if (ident->result_code != IDENT_AUTH_OKAY) {
        printf("      Capabilities: ");
        result = ident_query_error("CAPABILITIES", "USER-INTERACTION", ident);
        if (result == IDENT_AUTH_OKAY)
            printf(" USER-INTERACTION");
        printf("\r\n");
        printf("        Mechanisms: ");
#ifdef HAVE_KRB4
        result = ident_query_error("AUTH-MECH", "KERBEROS_V4", ident);
        if (result == IDENT_AUTH_OKAY)
            printf(" KERBEROS_V4");
#endif
        result = ident_query_error("AUTH-MECH", "GSSAPI", ident);
        if (result == IDENT_AUTH_OKAY)
            printf(" GSSAPI");
        printf("\r\n");
    }
    printf("\r\n");
    fflush(stdout);
}


int
main(int argc, char *argv[])
{
    IDENT *ident;
    int option;
    const char *srvtab = "/etc/leland/srvtab.ident";
    int interactive = 1;
    char *env;

    chdir("/tmp");
    printf("%s test server\r\n\r\n", PACKAGE_STRING);

    while ((option = getopt(argc, argv, "k:ns:w")) != EOF) {
        switch (option) {
        case 'k':
            env = malloc(strlen("KRB5_KTNAME=") + strlen(optarg) + 1);
            if (env == NULL) {
                fprintf(stderr, "ident: cannot allocate memory\n");
                exit(1);
            }
            strcpy(env, "KRB5_KTNAME=");
            strcat(env, optarg);
            if (putenv(env) != 0) {
                fprintf(stderr, "ident: cannot set keytab name\n");
                exit(1);
            }
            break;
        case 'n':
            interactive = 0;
            break;
        case 's':
            srvtab = optarg;
            break;
        case 'w':
            printf("Sleeping... run gdb attach %d\n", getpid());
            fflush(stdout);
            sleep(45);
            break;
        default:
            exit(1);
        }
    }

#ifdef HAVE_KRB4
    /* Testing Kerberos v4. */
    if (interactive) {
        if (ident_set_authtype("KERBEROS_V4", srvtab) != IDENT_AUTH_OKAY)
            fprintf(stderr, "ident: cannot set KERBEROS_V4\n");
        ident = ident_lookup(fileno(stdin), 30);
        print_results("KERBEROS_V4", ident);
    }

    /* Testing Kerberos v4, USER-INTERACTION=NO. */
    if (!interactive) {
        if (ident_set_authflag("USER-INTERACTION", "NO") != IDENT_AUTH_OKAY)
            fprintf(stderr, "ident: cannot set USER-INTERACTION=NO\n");
        ident = ident_lookup(fileno(stdin), 30);
        print_results("KERBEROS_V4, USER-INTERACTION=NO", ident);
        ident_free(ident);
    }

    /* Testing Kerberos v4, USER-INTERACTION=YES. */
    if (ident_set_authflag("USER-INTERACTION", "YES") != IDENT_AUTH_OKAY)
        fprintf(stderr, "ident: cannot set USER-INTERACTION=YES\n");
    ident = ident_lookup(fileno(stdin), 30);
    print_results("KERBEROS_V4, USER-INTERACTION=YES", ident);
#endif

    /* Testing GSSAPI and Kerberos v5. */
    if (ident_set_authtype("GSSAPI", NULL) != IDENT_AUTH_OKAY)
        fprintf(stderr, "ident: cannot set GSSAPI\n");
    ident = ident_lookup(fileno(stdin), 30);
    print_results("GSSAPI", ident);
    ident_free(ident);

    /* Done. */
    exit(0);
}
