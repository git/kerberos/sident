/*  $Id: ident.c 2429 2006-02-08 20:01:28Z rra $
**
**  High-level interface to the ident library.
**
**  Written by Pdr Emanuelsson <pell@lysator.liu.se>
**  With modifications by Peter Eriksson <pen@lysator.liu.se>
*/

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>

/* BSDI needs <netinet/in.h> before <arpa/inet.h>. */
#include <netinet/in.h>
#include <arpa/inet.h>

#include "sident.h"

#define MAX_PORT 65536
#define VALID_PORT(port) ((port) > 1 && (port) < MAX_PORT)

extern void id_auth_free();


/*
**  Do a complete ident query and return the result as an IDENT struct.
**  Assumes that the client to query is whatever client is attached to the
**  provided file descriptor.  The timeout is given in seconds.
*/
IDENT *
ident_lookup(int fd, int timeout)
{
    struct sockaddr_in localaddr, remoteaddr;
    socklen_t len;

    len = sizeof(remoteaddr);
    if (getpeername(fd, (struct sockaddr *) &remoteaddr, &len) < 0)
        return 0;
    len = sizeof(localaddr);
    if (getsockname(fd, (struct sockaddr *) &localaddr, &len) < 0)
        return 0;
    return ident_query(&localaddr.sin_addr, &remoteaddr.sin_addr,
                       (int) ntohs(remoteaddr.sin_port),
                       (int) ntohs(localaddr.sin_port), timeout);
}


/*
**  Same as ident_lookup, but takes the local address and port and the remote
**  address and port.  This function can be run by a process completely
**  unrelated to the one to which the client is connected, if need be.
*/
IDENT *
ident_query(struct in_addr *laddr, struct in_addr *raddr,
            int resp_port, int req_port, int timeout)
{
    int res;
    ident_t *id;
    struct timeval timout;
    IDENT *ident = NULL;
    struct ident_auth_client_data *auth_data;
    char tmp[IDENT_BUFFER_SIZE];

    /* Do some sanity checks on the args */
    if (!VALID_PORT(resp_port) || !VALID_PORT(req_port) || timeout < 0) {
        errno = EINVAL;
        return NULL;
    }

    timout.tv_sec = timeout;
    timout.tv_usec = 0;

    auth_data = id_auth_init(NULL);
    auth_data->local_addr = laddr;
    auth_data->remote_addr = raddr;

    if (timeout > 0)
        id = id_open(laddr, raddr, &timout);
    else
        id = id_open(laddr, raddr, NULL);

    if (id == NULL) {
        errno = EINVAL;
        return NULL;
    }

    if (timeout)
        res = id_query(id, resp_port, req_port, &timout, auth_data);
    else
        res = id_query(id, resp_port, req_port, NULL, auth_data);

    if (res < 0) {
        id_auth_free(auth_data);
        id_close(id);
        return NULL;
    }

    ident = malloc(sizeof(IDENT));
    if (ident == NULL) {
        id_auth_free(auth_data);
        id_close(id);
        return NULL;
    }

    if (timeout)
        res = id_parse(id, &timout, &ident->resp_port, &ident->req_port,
                       &ident->identifier, &ident->opsys, &ident->charset,
                       auth_data);
    else
        res = id_parse(id, NULL, &ident->resp_port, &ident->req_port,
                       &ident->identifier, &ident->opsys, &ident->charset,
                       auth_data);

    /* Copy over the principal, if any, first, since this is another memory
       allocation that could potentially fail and change the result code. */
    if (auth_data->user_principal != NULL) {
        ident->principal = strdup(auth_data->user_principal);
        if (ident->principal == NULL)
            res = IDENT_SYSTEM_ERROR;
    }

    /* Check to see if we succeeded.  If we didn't, cobble up an error reply
       to stuff into the identifier field. */
    if (res != IDENT_AUTH_OKAY) {
        if (ident->identifier == NULL)
            sprintf(tmp, "%s:NULL", ident_err_txt[res]);
        else {
            sprintf(tmp, "%s:%s", ident_err_txt[res], ident->identifier);
            free(ident->identifier);
        }
        ident->identifier = strdup(tmp);
    }

    /* Success.  Set the rest of the ident struct, free our internal
       structures, and return. */
    ident->result_code = res;
    ident->expires = auth_data->expires;
    id_auth_free(auth_data);
    id_close(id);
    return ident;
}


/*
**  The simplest of the lookup calls.  Assumes that fd points to an open
**  network connection and does an ident callback to the system on the other
**  end, asking for the identity of the user opening that connection.  Returns
**  only the identity string in newly allocated memory.  timeout specifies the
**  timout in seconds.  If the protocol failed or the user could not be
**  authenticated, returns NULL.
*/
char *
ident_id(int fd, int timeout)
{
    IDENT *ident;
    char *id = NULL;

    ident = ident_lookup(fd, timeout);
    if (ident == NULL)
        return NULL;
    if (ident->identifier == NULL || *ident->identifier == '\0') {
        ident_free(ident);
        return NULL;
    }
    if (ident->result_code != IDENT_AUTH_OKAY) {
        ident_free(ident);
        return NULL;
    }
    id = strdup(ident->identifier);
    ident_free(ident);
    return id;
}


/*
**  Free an IDENT struct and all of the allocated memory that it points to.
*/
void
ident_free(IDENT *id)
{
    if (id == NULL)
        return;
    if (id->identifier != NULL)
        free(id->identifier);
    if (id->opsys != NULL)
        free(id->opsys);
    if (id->charset != NULL)
        free(id->charset);
    free(id);
}
