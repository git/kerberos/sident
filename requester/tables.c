/*  $Id: tables.c 2430 2006-02-08 21:32:45Z rra $
**
**  String tables used by library routines and made available by sident.h.
**  The ident_err_txt table must match the enum id_codes.
*/

/* Text strings corresponding to various errors.  The string values are in
   many cases madated by the protocol. */
const char *ident_err_txt[] = {
    "NO-ERROR",                 /* 0 */
    "AUTH-FAILURE",
    "AUTH-NOT-SUPPORTED",
    "USER-CANT-AUTH",
    "INVALID-AUTH-RESP-INFO",
    "INVALID-AUTH-REQ-INFO",
    "NO-USER",
    "UNKNOWN-ERROR",            /* 7 */
    "INVALID-PORT",
    "HIDDEN-USER",
    "USER-WONT-AUTH",           /* Deprecated */
    "INTERNAL-ERROR",           /* 11 */
    "NO-MUTUAL-AUTH",
    "MUTUAL-AUTH-FAIL",
    "SYSTEM-ERROR",             /* 14 */
    "FLAG-NOT-SUPPORTED",
    "INVALID-FLAG-VALUE",       /* 16 */
    "TIMEOUT"
};

/* Authentication flags recognized by the KERBEROS_V4 and GSSAPI
   authentication protocol. */
const char *ident_krb_auth_flags[] = {
    "USER-INTERACTION"
};
