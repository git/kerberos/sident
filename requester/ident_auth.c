/*  $Id: ident_auth.c 2430 2006-02-08 21:32:45Z rra $
**
**  SASL-based authentication mechanisms for ident.
**
**  Written by Booker Bense <bbense@stanford.edu>
**  GSS-API support added by Russ Allbery <rra@stanford.edu>
**
**  Much of this code was originally taken from acte_krb.c in the Cyrus IMAP
**  distribution.  At the time, the copyright and license statement was as
**  follows:
**
**      (C) Copyright 1994 by Carnegie Mellon University
**
**                      All Rights Reserved
**
**  Permission to use, copy, modify, distribute, and sell this software and
**  its documentation for any purpose is hereby granted without fee, provided
**  that the above copyright notice appear in all copies and that both that
**  copyright notice and this permission notice appear in supporting
**  documentation, and that the name of Carnegie Mellon University not be used
**  in advertising or publicity pertaining to distribution of the software
**  without specific, written prior permission.  Carnegie Mellon University
**  makes no representations about the suitability of this software for any
**  purpose.  It is provided "as is" without express or implied warranty.
**
**  CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
**  SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
**  IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE FOR ANY SPECIAL,
**  INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
**  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
**  OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
**  PERFORMANCE OF THIS SOFTWARE.
*/

#include "config.h"

#include <ctype.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pwd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef HAVE_KRB4
# ifdef HAVE_KERBEROSIV_KRB_H
#  include <kerberosIV/krb.h>
# else
#  include <krb.h>
# endif
#endif

#include "sident.h"

/* Exported definition of requester authentication mechanism */
static char *null_requester_start();
static int null_requester_auth();
static void null_free_state();
static char *trivial_requester_start();
static int trivial_requester_auth();
static int null_set_flag();
static int null_get_flag();
#ifdef HAVE_KRB4
static char *krb_requester_start();
static int krb_requester_auth();
static void krb_free_state();
#endif
static int krb_set_flag();
static int krb_get_flag();

/* GSS-API routines. */
extern char *gssapi_requester_start();
extern int gssapi_requester_auth();
extern void gssapi_free_state();

extern char *sfgets();

#ifdef HAVE_KRB4
struct ident_requester_auth krb_ident_auth = {
    "KERBEROS_V4",
    NULL,
    NULL,
    NULL,
    krb_requester_start,
    krb_requester_auth,
    krb_free_state,
    krb_set_flag,
    krb_get_flag
};
#endif

struct ident_requester_auth gssapi_ident_auth = {
    "GSSAPI",
    NULL,
    NULL,
    NULL,
    gssapi_requester_start,
    gssapi_requester_auth,
    gssapi_free_state,
    krb_set_flag,
    krb_get_flag
};

struct ident_requester_auth otp_ident_auth = {
    "OTP_RFC1938",
    NULL,
    NULL,
    NULL,
    null_requester_start,
    null_requester_auth,
    null_free_state,
    null_set_flag,
    null_get_flag
};

struct ident_requester_auth trivial_ident_auth = {
    "TRIVIAL",
    NULL,
    NULL,
    NULL,
    trivial_requester_start,
    trivial_requester_auth,
    null_free_state,
    null_set_flag,
    null_get_flag
};

#ifdef HAVE_KRB4
# define IDENT_METHODS_COUNT 4
#else
# define IDENT_METHODS_COUNT 3
#endif

/* TRIVIAL method must always be the last method. */
#define IDENT_TRIVIAL_METHOD (IDENT_METHODS_COUNT - 1)

struct ident_requester_auth *auth_methods[IDENT_METHODS_COUNT] = {
#ifdef HAVE_KRB4
    &krb_ident_auth,
#endif
    &gssapi_ident_auth,
    &otp_ident_auth,
    &trivial_ident_auth
};

/* Start with trivial as the default.  This probably isn't right, but neither
   GSS-API nor Kerberos v4 are obviously better choices. */
struct ident_requester_auth *global_auth_method = &trivial_ident_auth;

/* Srvtab filename. */
static char *srvtab = "";


/*
**  Sets the type of authentication to request of the remote system.  The
**  second parameter is type-specific initialization data.  For KERBEROS_V4,
**  this should be the full path to the srvtab file that contains the key for
**  the ident.hostname Kerberos principal.  Neither TRIVIAL nor GSSAPI take
**  any data.
**
**  Returns IDENT_AUTH_OKAY on success and IDENT_AUTH_NOT_SUPPORTED on
**  failure.
*/
int
ident_set_authtype(char *auth_type, void *auth_init_data)
{
    int i;

    for (i = 0; i < IDENT_METHODS_COUNT; i++)
        if (strcmp(auth_type, auth_methods[i]->auth_method) == 0) {
            global_auth_method = auth_methods[i];
            auth_methods[i]->auth_method_init = auth_init_data;
            return IDENT_AUTH_OKAY;
        }
    return IDENT_AUTH_NOT_SUPPORTED;
}


/*
**  Set an authentication flag, which has to be handled by each method.
*/
int
ident_set_authflag(char *flag, char *value)
{
    return global_auth_method->set_flag(flag, value);
}


/*
**  Get the current value of an authentication flag.  Puts a pointer to an
**  internal value that must not be manipulated into value and returns a
**  status code.
*/
int
ident_get_authflag(char *flag, char **value)
{
    return global_auth_method->get_flag(flag, value);
}


/*
**  Interogates the error string returned from the responder for a given flag
**  and corresponding value.  The two currently defined flags are CAPABILITIES
**  and AUTH-MECH.  See the S/Ident draft for the corresponding values.  The
**  responder uses the extended error codes to allow the requester to attempt
**  to reformulate its call in an attempt to complete a successful
**  transaction.  This can be used to negotiate authentication methods, as
**  well as to distinguish between responders capable of user interaction and
**  those that are not capable.
**
**  It takes a keyword to search for, the value that keyword should have, and
**  the IDENT structure representing the error response.  It returns
**  IDENT_AUTH_OKAY if that flag/value pair is found and IDENT_AUTH_FAIL if it
**  is not.
*/
int
ident_query_error(char *keyword, char *value, IDENT *ident)
{
    char *e_string, *extension, *tmp;

    if (ident->result_code == IDENT_AUTH_OKAY)
        return IDENT_AUTH_FAIL;

    /* Format is error_code_string:everything in response after ERROR. */
    e_string = ident->identifier;
    if (e_string == NULL)
        return IDENT_AUTH_FAIL;
    tmp = strchr(e_string, ':');
    if (tmp == NULL)
        return IDENT_AUTH_FAIL;
    extension = strchr(tmp, ':');
    if (extension == NULL)
        return IDENT_AUTH_FAIL;
    tmp = strstr(extension, keyword);
    if (tmp == NULL)
        return IDENT_AUTH_FAIL;
    e_string = tmp;
    tmp += strlen(keyword);
    if (*tmp != '=')
        return IDENT_AUTH_FAIL;
    e_string = ++tmp;
    if (strstr(e_string, value) == NULL)
        return IDENT_AUTH_FAIL;
    return IDENT_AUTH_OKAY;
}


/*
**  Initialize an ident_auth_client_data struct.  Note that the argument is
**  completely ignored.
*/
struct ident_auth_client_data *
id_auth_init(struct ident_auth_client_data *auth_data)
{
    auth_data = calloc(1, sizeof(struct ident_auth_client_data));
    if (auth_data == NULL)
        return NULL;
    auth_data->auth_struct = global_auth_method;
    return auth_data;
}


/*
**  Free an ident_auth_client_data struct.
*/
void
id_auth_free(struct ident_auth_client_data *auth_data)
{
    if (auth_data->auth_method_data != NULL)
        free(auth_data->auth_method_data);
    free(auth_data);
}


/*
**  Parse the authentication portion of a response from the responder and
**  invoke the appropriate handler method for the type of response.  Puts an
**  ident_requester_auth struct corresponding to the authentication mechanism
**  used into auth_struct.
*/
int
parse_auth(char *line, struct ident_requester_auth **auth_struct)
{
    int i, rcode;
    char auth_method[128];

    *auth_struct = NULL;
    if (line == NULL) {
        *auth_struct = auth_methods[IDENT_TRIVIAL_METHOD];
        return IDENT_AUTH_OKAY;
    }
    rcode = sscanf(line, " %32[^ \t\n\r,]", auth_method);
    for (i = 0; i < IDENT_METHODS_COUNT; i++)
        if (strcmp(auth_method, auth_methods[i]->auth_method) == 0) {
            *auth_struct = auth_methods[i];
            return IDENT_AUTH_OKAY;
        }
    return IDENT_INVALID_REQ_INFO;
}


/* Tables for encoding and decoding base64. */
static const char basis_64[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
#define XX 127
static const char index_64[256] = {
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,62, XX,XX,XX,63,
    52,53,54,55, 56,57,58,59, 60,61,XX,XX, XX,XX,XX,XX,
    XX, 0, 1, 2,  3, 4, 5, 6,  7, 8, 9,10, 11,12,13,14,
    15,16,17,18, 19,20,21,22, 23,24,25,XX, XX,XX,XX,XX,
    XX,26,27,28, 29,30,31,32, 33,34,35,36, 37,38,39,40,
    41,42,43,44, 45,46,47,48, 49,50,51,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
};
#define CHAR64(c)  (index_64[(unsigned char)(c)])

/*
**  Decode in-place a buffer encoded in base64.  Returns the length of the
**  decoded data or -1 on an error.
*/
int
ident_decodebase64(char *input)
{
    int len = 0;
    unsigned char *output = (unsigned char *)input;
    int c1, c2, c3, c4;

    while (*input) {
        c1 = *input++;
        if (CHAR64(c1) == XX)
            return -1;
        c2 = *input++;
        if (CHAR64(c2) == XX)
            return -1;
        c3 = *input++;
        if (c3 != '=' && CHAR64(c3) == XX)
            return -1;
        c4 = *input++;
        if (c4 != '=' && CHAR64(c4) == XX)
            return -1;
        *output++ = (CHAR64(c1) << 2) | (CHAR64(c2) >> 4);
        ++len;
        if (c3 == '=')
            break;
        *output++ = ((CHAR64(c2) << 4) & 0xf0) | (CHAR64(c3) >> 2);
        ++len;
        if (c4 == '=')
            break;
        *output++ = ((CHAR64(c3) << 6) & 0xc0) | CHAR64(c4);
        ++len;
    }

    return len;
}

/*
**  Encode data into base64.  Takes the input buffer, the output buffer, and
**  the length of the data in the input buffer.  The output buffer must be
**  long enough to hold the encoded input.
*/
void
ident_writebase64(char *input, char *output, int len)
{
    char buf[IDENT_BUFFER_SIZE];
    char *outptr;
    int buflen = 0;
    int c1, c2, c3;

    outptr = output;
    while (len > 0) {
        if (buflen >= sizeof(buf) - 4) {
            strncpy(outptr, buf, buflen);
            buflen = 0;
            outptr += buflen;
        }

        c1 = (unsigned char) *input++;
        buf[buflen++] = basis_64[c1 >> 2];

        if (--len == 0)
            c2 = 0;
        else
            c2 = (unsigned char) *input++;
        buf[buflen++] = basis_64[((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4)];

        if (len == 0) {
            buf[buflen++] = '=';
            buf[buflen++] = '=';
            break;
        }

        if (--len == 0)
            c3 = 0;
        else
            c3 = (unsigned char) *input++;
        buf[buflen++] = basis_64[((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6)];

        if (len == 0) {
            buf[buflen++] = '=';
            break;
        }

        --len;
        buf[buflen++] = basis_64[c3 & 0x3F];
    }

    if (buflen >= sizeof(buf) - 2) {
        strncpy(outptr, buf, buflen);
        outptr += buflen;
        buflen = 0;
    }
    buf[buflen++] = '\0';
    strcpy(outptr, buf);
}


/*
**  Send a message to the remote host, following the timeout in auth_data.
**  Returns the number of bytes written to the remote host, or -1 on error.
*/
int
do_out(char *string, struct ident_auth_client_data *auth_data)
{
    RETSIGTYPE (*old_sig)(int);
    int res;
    char buf[IDENT_BUFFER_SIZE];
    fd_set ws;

    if (auth_data == NULL)
        return -1;
    sprintf(buf, "%d , %d : AUTHENTICATE : %s\r\n", auth_data->responder_port,
            auth_data->requester_port, string);

#ifdef DEBUG
    printf("Sending.. \n%s",buf) ;
#endif

    if (auth_data->timeout != NULL) {
        FD_ZERO(&ws);
        FD_SET(auth_data->fd, &ws);
        res = select(FD_SETSIZE, NULL, &ws, NULL, auth_data->timeout);
        if (res < 0)
            return -1;
        else if (res == 0) {
            errno = ETIMEDOUT;
            return -1;
        }
    }
    old_sig = signal(SIGPIPE, SIG_IGN);
    res = write(auth_data->fd, buf, strlen(buf));
    signal(SIGPIPE, old_sig);
    return res;
}


/*
**  Handle setting a flag.  Right now, USER-INTERACTION is the only thing
**  that's supported, so no time has been spent to make this really work
**  right.  Note that both the Kerberos v4 and GSS-API mechanisms use this
**  implementation.
*/
static int
krb_set_flag(char *flag, char *value)
{
    int i;

    if (flag != NULL && value != NULL)
        for (i = 0; i < IDENT_MAX_KRB_AUTH_FLAG; i++)
            if (strcmp(flag, ident_krb_auth_flags[i]) == 0) {
                if (strcmp(value, "YES") == 0)
                    global_auth_method->auth_flags = "USER-INTERACTION=YES";
                else if (strncmp(value, "NO", 2) == 0)
                    global_auth_method->auth_flags = "USER-INTERACTION=NO";
                else
                    return IDENT_INVALID_FLAG_VALUE;
                return IDENT_AUTH_OKAY;
            }
    return IDENT_FLAG_NOT_SUPPORTED;
}


/*
**  Handle retrieving a flag.  Note that both Kerberos v4 and GSS-API
**  mechanisms use this implementation.
*/
static int
krb_get_flag(char *flag, char **value)
{
    int i;
    char *ptr_flag, *ret_ptr;

    *value = NULL;
    if (flag != NULL)
        for (i = 0; i < IDENT_MAX_KRB_AUTH_FLAG; i++)
            if (strcmp(flag, ident_krb_auth_flags[i]) == 0) {
                ptr_flag = strstr(global_auth_method->auth_flags, flag);
                if (ptr_flag != NULL) {
                    ret_ptr = strchr(ptr_flag, '=');
                    if (ret_ptr != NULL)
                        *value = ret_ptr + 1;
                }
                return IDENT_AUTH_OKAY;
            }

    /* Not a flag we recognize. */
    return IDENT_FLAG_NOT_SUPPORTED;
}


/* Skip all the KERBEROS_V4 implementation unless we're building with v4
   support. */

#ifdef HAVE_KRB4

/* Private state used by this mechanism */
struct krb_state {
    char service[MAX_K_NAME_SZ + 1];
    int authstepno;
    des_cblock session;         /* Our session key */
    des_key_schedule schedule;  /* Schedule for our session key */
    int32_t challenge;
    char string[2048];          /* Store built responses */
    AUTH_DAT kdata;
};


/*
**  Free the space used by the opaque mechanism state pointer.
*/
static void
krb_free_state(void *state)
{
    memset(state, 0, sizeof(struct krb_state));
    free(state);
}


/*
**  Start the requester side of a Kerberos v4 authentication exhange.  Returns
**  the string that should be sent to the remote host.
*/
static char *
krb_requester_start(struct ident_auth_client_data *auth_data)
{
    struct krb_state *kstate;
    char *p;
    char tmp[2048];
    int32_t k;

    kstate = malloc(sizeof(struct krb_state));
    if (kstate == NULL)
        return NULL;
    strcpy(kstate->service, IDENT_AUTH_KRB_SERVICE);

    /* Just need to return a string that has an auth type and a challenge.  We
       could use more randomness here. */
    kstate->authstepno = 0;
    p = kstate->string;
    kstate->challenge = rand() ^ time(NULL) ^ getpid();
#ifdef DEBUG
    printf("Challenge is %ld\n", kstate->challenge);
#endif
    k = htonl(kstate->challenge);
    ident_writebase64((char *) &k, tmp, sizeof(k));
    if (global_auth_method->auth_flags == NULL)
        sprintf(p, "%s, %s", auth_data->auth_struct->auth_method, tmp);
    else
        sprintf(p, "%s, %s, %s", auth_data->auth_struct->auth_method, tmp,
                global_auth_method->auth_flags);
    auth_data->auth_method_data = kstate;
    return p;
}


/*
**  Perform the requester-side authentication protocol exchange.  This is the
**  main loop, which continues until the authentication exchange is complete
**  and then returns the status, after filling out the auth_data struct.
*/
static int
krb_requester_auth(struct ident_auth_client_data *auth_data)
{
    static KTEXT_ST authent;
    struct krb_state *kstate = auth_data->auth_method_data;
    char tmp[IDENT_BUFFER_SIZE];
    char output[IDENT_BUFFER_SIZE];
    char *tmp_ptr;
    char instance[INST_SZ];
    char uid[INST_SZ];
    char realm[REALM_SZ];
    int32_t from_addr;
    IDENT_INT16 i, len , mutual_auth_flag, uid_len, requester_port;
    IDENT_INT16 responder_port, munch;
    int code, rcode;

    while (kstate->authstepno > -1) {
        switch (kstate->authstepno++) {
        case 0:
            /* Get kerberos data. */
            if (strlen(auth_data->authenticate_data) > MIN_AUTH_LEN) {
                rcode = sscanf(auth_data->authenticate_data,
                               " KERBEROS_V4 , %[A-Za-z0-9+/=]", tmp) ;
                if (rcode != 1) {
                    kstate->authstepno = -1;
                    return IDENT_INVALID_RESP_INFO;
                }
            } else {
                kstate->authstepno = -1;
                return IDENT_INVALID_RESP_INFO;
            }
            srvtab = auth_data->auth_struct->auth_method_init;
            from_addr = auth_data->remote_addr->s_addr;
            rcode = ident_decodebase64(tmp);

            /* Client gave us ticket and authenticator.  Parse it and flesh
               out our structures. */
            munch = sizeof(IDENT_INT16);
            memcpy(&i, tmp, munch);
            authent.length = ntohs(i);

            /* Hey, you'd do it this way too if you learned C on a Cray
               YMP. */
            tmp_ptr = &tmp[munch];
            memcpy(authent.dat, tmp_ptr, authent.length);
            authent.mbz = 0;
            strcpy(instance, "*");
            code = krb_rd_req(&authent, kstate->service, instance, from_addr,
                              &kstate->kdata, srvtab);

#ifdef DEBUG
            printf("krb_err_code %d , %s\n",code, krb_err_txt[code]);
#endif

            if (code != 0) {
                kstate->authstepno = -1;
                switch (code) {
                case RD_AP_BADD: /* This is common on dynamic ip clients */
                case RD_AP_TIME:
                case RD_AP_EXP:
                    return IDENT_USER_CANT_AUTH;
                    break;
                default:
                    return IDENT_INVALID_RESP_INFO;
                }
            }
            if (kstate->kdata.checksum != kstate->challenge) {
                kstate->authstepno = -1;
                return IDENT_MUTUAL_AUTH_FAIL;
            }

            /* Pull off the expiration time. */
            auth_data->expires = krb_life_to_time(kstate->kdata.time_sec,
                                                  kstate->kdata.life);
            memcpy(kstate->session, kstate->kdata.session,
                   sizeof(des_cblock));
            des_key_sched(kstate->session, kstate->schedule);

            /* Now get rest of info from tmp and check for integrity. */
            tmp_ptr = &(tmp[authent.length + munch]) ;
            memcpy(&i, tmp_ptr, munch);
            len = ntohs(i);
            tmp_ptr++;
            tmp_ptr++;
            des_pcbc_encrypt((des_cblock *) tmp_ptr, (des_cblock *) output,
                             len, kstate->schedule, &(kstate->session), 0);

            memcpy(&mutual_auth_flag, output, munch);
            tmp_ptr = output + munch;

            memcpy(&responder_port, tmp_ptr, munch);
            tmp_ptr += munch;
            memcpy(&requester_port, tmp_ptr, munch);
            tmp_ptr += munch;
            memcpy(&uid_len, tmp_ptr, munch);
            tmp_ptr += munch;
            mutual_auth_flag = ntohs(mutual_auth_flag);
            requester_port = ntohs(requester_port);
            responder_port = ntohs(responder_port);
            uid_len = ntohs(uid_len);

            if ((requester_port != auth_data->requester_port)
                || (responder_port != auth_data->responder_port))
                return IDENT_INVALID_RESP_INFO;
            tmp_ptr = &output[4 * munch];
            memcpy(uid, tmp_ptr, uid_len);
            uid[uid_len] = '\0';

            /* Build user.inst@realm string and dump into user_ident. */
            sprintf(auth_data->user_ident, "%s.%s@%s:%s", kstate->kdata.pname,
                    kstate->kdata.pinst, kstate->kdata.prealm, uid);

            /* Fill in principal if we should. */
            if ((krb_get_lrealm(realm, 1) == 0)
                && (strcmp(kstate->kdata.prealm, realm) == 0)
                && (kstate->kdata.pinst[0] == '\0'))
                strcpy(auth_data->user_principal, kstate->kdata.pname);

            if (mutual_auth_flag & MUTUAL_AUTH_BIT)
                return IDENT_AUTH_OKAY;

            break;

        case 1:
            /* Send back mutual auth reply. */
            *(int *)output = htonl(kstate->challenge + 1);
            tmp_ptr = output + sizeof(int32_t);
            memset(tmp_ptr, '\0', sizeof(des_cblock) - sizeof(int32_t));
            des_ecb_encrypt((des_cblock *) output, (des_cblock *) output,
                            kstate->schedule, 1);
            ident_writebase64(output, tmp, sizeof(des_cblock));
            sprintf(output, "%s, %s", auth_data->auth_struct->auth_method,
                    tmp);
            do_out(output, auth_data);
            return IDENT_AUTH_OKAY;
            break;

        default:
            kstate->authstepno = -1;
            return IDENT_SYSTEM_ERROR;
        }
    }
    return IDENT_AUTH_FAIL;
}

#endif /* HAVE_KRB4 */

/*
**  For the trivial authentication mechanism, we don't have to do anything in
**  particular since there are no special reply blocks to parse.
*/
static char *
trivial_requester_start(struct ident_auth_data *auth_data)
{
    return "";
}

static int
trivial_requester_auth(struct ident_auth_data *auth_data)
{
    return IDENT_AUTH_OKAY;
}

/* Null methods; just always return true (shouldn't these always fail?). */
static int null_set_flag(char *flag, char *value)  { return IDENT_AUTH_OKAY; }
static int null_get_flag(char *flag, char **value) { return IDENT_AUTH_OKAY; }

static char *
null_requester_start(struct ident_auth_data *auth_data)
{
    return NULL;
}

static int
null_requester_auth(struct ident_auth_data *auth_data)
{
    return IDENT_AUTH_NOT_SUPPORTED;
}

static void
null_free_state(void *state)
{
    return;
}
