/*  $Id: support.c 550 2004-06-17 01:27:41Z eagle $
**
**  Support utilities for libsident.
**
**  Written by Pr Emanuelsson <pell@lysator.liu.se>
**  Modifications by Peter Eriksson <pen@lysator.liu.se>
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sident.h"

char *
id_strtok(char *cp, char *cs, char *dc)
{
    static char *bp = 0;

    if (cp != NULL)
        bp = cp;

    /* No delimitor cs - return whole buffer and point at end. */
    if (cs == NULL) {
        while (*bp)
            bp++;
        return cs;
    }

    /* Skip leading spaces. */
    while (isspace(*bp))
        bp++;

    /* No token found? */
    if (*bp == '\0')
        return 0;

    cp = bp;
    while (*bp && !strchr(cs, *bp))
        bp++;

    /* Remove trailing spaces. */
    *dc = *bp;
    for (dc = bp - 1; dc > cp && isspace(*dc); dc--)
        ;
    *++dc = '\0';

    bp++;

    return cp;
}
