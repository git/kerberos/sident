/*  $Id: itest.c 545 2004-06-14 04:45:50Z eagle $
**
**  Simple but effective hack to test the kernel module.
**
**  Written by Casper Dik <casper@fwi.uva.nl>
**
**  It takes on stdin the output from "netstat -f inet -n | grep ESTAB" in
**  either Solaris 2.x (non-standard formats can easily be converted to this)
**
**      laddr.lport faddr.fport .....
**
**  or BSD 4.x (the defacto standard netstat output):
**
**      tcp <num> <num>  laddr.lport faddr.fport
**
**  format.  Optionally, the delimiter between the address and the port may be
**  a colon instead.
**
**  The output must be numeric, as non-numeric output is truncated when
**  hostnames get too long and ambiguous.  And we don't want netstat to first
**  convert numbers to names and then this program to convert names back to
**  numbers.
*/

#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <syslog.h>

/* BSDI needs <netinet/in.h> before <arpa/inet.h>. */
#include <netinet/in.h>
#include <arpa/inet.h>

#include "identd.h"
#include "pidentd.h"

/* External references that are usually resolved from identd.c. */
int debug_flag = 1;
int syslog_flag = 1;

int
main(void)
{
    char buf[500];
    void *ka_ptr;
    struct kernel *kinfo_ptr;

    /* Some things just always syslog, so try to at least put a reasonable
       program name on them.  This needs to be fixed. */
    openlog("itest", LOG_PID, LOG_DAEMON);

    /* Open the kernel. */
    if (ka_open(&ka_ptr) != 0) {
        fprintf(stderr, "ka_open failed\n");
        exit(1);
    }

    /* For each input string, parse it and then look it up in the kernel. */
    while (fgets(buf, sizeof(buf), stdin) != NULL) {
        char *loc, *rem, *tmp;
        unsigned short lport, fport;
        struct in_addr faddr, laddr;
        int i;
        struct passwd *pwd;
        char buf2[sizeof(buf)];
        uid_t uid;

        strcpy(buf2, buf);

        loc = strtok(buf, " \t");
        if (strcmp(loc, "tcp") == 0)
            for (i = 0; i < 3; i++)
                loc = strtok(NULL, " \t");
        rem = strtok(NULL, " \t");
        if (loc == NULL || rem == NULL) {
            fprintf(stderr,"Malformed line: %s\n", buf2);
            continue;
        }

        /* Parse remote, local address. */
        tmp = strrchr(loc, ':');
        if (tmp == NULL)
            tmp = strrchr(loc, '.');
        if (tmp == NULL) {
            fprintf(stderr,"Malformed line: %s\n", buf2);
            continue;
        }
        *tmp++ = '\0';
        laddr.s_addr = inet_addr(loc);
        lport = atoi(tmp);
        tmp = strrchr(rem, ':');
        if (tmp == NULL)
            tmp = strrchr(loc, '.');
        if (tmp == NULL) {
            fprintf(stderr,"Malformed line: %s\n", buf2);
            continue;
        }
        *tmp++ = '\0';
        fport = atoi(tmp);
        faddr.s_addr = inet_addr(rem);

        /* Flesh out the kernel lookup request. */
        kinfo_ptr = kernel_alloc();
        kinfo_ptr->local.sin_port = htons(lport);
        kinfo_ptr->remote.sin_port = htons(fport);
        memcpy(&kinfo_ptr->local.sin_addr, &laddr, sizeof(struct in_addr));
        memcpy(&kinfo_ptr->remote.sin_addr, &faddr, sizeof(struct in_addr));
        kinfo_ptr->local.sin_family = AF_INET;
        kinfo_ptr->remote.sin_family = AF_INET;

        /* Do the kernel lookup. */
        if (ka_lookup(ka_ptr, kinfo_ptr) < 0) {
            fprintf(stderr, "ka_lookup failed\n");
            exit(1);
        }
        if (kinfo_ptr->status == 0) {
            fprintf(stderr, "Connection not found\n");
            continue;
        }

        /* Print out the results. */
        uid = (kinfo_ptr->ruid == NO_UID) ? kinfo_ptr->euid : kinfo_ptr->ruid;
        pwd = getpwuid(uid);
        if (pwd != NULL)
            printf("%-8.8s", pwd->pw_name);
        else
            printf("%-8.8d", kinfo_ptr->ruid);
        printf(" \t%-13s\t%-4d\t%-13s\t%-4d\tPID=%d\tCMD=%s\tCMD+ARG=%s\n",
               loc, lport, rem, fport, kinfo_ptr->pid, kinfo_ptr->cmd,
               kinfo_ptr->argv);
    }
    return 0;
}
