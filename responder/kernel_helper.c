/*  $Id: kernel_helper.c 556 2004-06-18 08:23:34Z eagle $
**
**  Glue routines for the pident kernel files.
**
**  This file provides the glue routines needed to use pidentd 3.0.* k_files
**  with the S/Ident responder code.
**
**  Written by Peter Eriksson <pen@lysator.liu.se>
**  With modifications for S/Ident by Booker C. Bense <bbense@stanford.edu>
**
**  Most of the s_* functions are covered by the following license:
**
**  Copyright (c) 1997-1999 Peter Eriksson <pen@lysator.liu.se>
**
**  This program is free software; you can redistribute it and/or modify it as
**  you wish - as long as you don't claim that you wrote it.
**
**  This program is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE.
*/

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <syslog.h>
#include <unistd.h>

#include "identd.h"
#include "pidentd.h"

/* Dummy variables that aren't used but that various kernel modules expect. */
const char *osinfo_build = "the current os";
int kernel_threads = 0;
int kernel_buffers = 0;
int kernel_attempts = 0;

/* osinfo_get is never actually used, since we don't use the ka_init function
   in the k_files.  Its just to make the compiler happy.  */
char *osinfo_get(char *os_current) { return NULL; }

/* Allocate the kernel struct that's used as an interface. */
struct kernel *
kernel_alloc(void)
{
    struct kernel *kp;
    kp = s_malloc(sizeof(struct kernel));
    kp->status = -1;
    kp->euid = NO_UID;
    kp->ruid = NO_UID;
    kp->pid = NO_PID;
    kp->cmd = NULL;
    kp->argv = NULL;
    return kp;
}

/* Free the kernel struct. */
void
kernel_free(struct kernel *kp)
{
    s_free(kp->cmd);
    s_free(kp->argv);
    s_free(kp);
}


/* The following functions are all copies or substitutes for the s_* functions
   provided in pidentd, since they are used by the kernel modules. */

/* abort replacement. */
void s_abort(void) { abort(); }


/*
**  Safe malloc replacement that always succeeds (if it doesn't, it logs an
**  error message to syslog and then aborts.  The buffer is zeroed out before
**  it's returned, similar to calloc.
*/
void *
s_malloc(size_t size)
{
    void *p;

    p = malloc(size);
    if (p == NULL) {
        if (debug_flag)
            fprintf(stderr, "s_malloc(%lu) failed - aborting\n",
                    (unsigned long) size);
        syslog(LOG_ERR, "malloc(%lu): %m", (unsigned long) size);
        s_abort();
    }
    memset(p, 0, size);
    return p;
}


/*
**  Safe free replacement that doesn't call the system free if the pointer is
**  NULL (some system free implementations can't handle that).
*/
void
s_free(void *p)
{
    if (p != NULL)
        free(p);
}


/*
**  Safe strdup replacement.
*/
char *
s_strdup(const char *s)
{
    char *ns;
    size_t len;

    if (s == NULL)
        return NULL;
    len = strlen(s) + 1;
    ns = malloc(len);
    if (ns == NULL) {
        syslog(LOG_ERR, "strdup(): malloc(%lu): %m", (unsigned long) len);
        s_abort();
    }
    memcpy(ns, s, len);
    return ns;
}

#ifndef ENOSR
# define ENOSR ENFILE
#endif

/*
**  Safe s_open replacement that repeats the call to open on EINTR and logs a
**  warning on errors that indicate too many open files.
*/
int
s_open(const char *path, int oflag, ...)
{
    int s;
    mode_t mode = 0;

    if (oflag & O_CREAT) {
        va_list ap;

        va_start(ap, oflag);
        /* FIXME: need to use widened form of mode_t here. */
        mode = va_arg(ap, int);
        va_end(ap);
    }

    do {
        s = open(path, oflag, mode);
    } while (s < 0 && errno == EINTR);

    /* Check for too many open files. */
    if (s < 0 && (errno == EMFILE || errno == ENFILE || errno == ENOMEM
                  || errno == ENOSR))
        syslog(LOG_WARNING, "s_open(\"%s\", 0%o): %m", path, oflag);
    return s;
}


/*
**  snprintf replacement that uses vsprintf if vsnprintf isn't available and
**  tries to detect a buffer overrun.  This can be replaced with a call to
**  vsnprintf once we have an snprintf replacement.
*/
int
s_snprintf(char *buf, size_t bufsize, const char *format, ...)
{
    va_list ap;
    int retcode;

    if (bufsize < 1) {
        syslog(LOG_ERR, "s_snprintf(..., %lu, ...): illegal bufsize",
               (unsigned long) bufsize);
        s_abort();
    }

    va_start(ap, format);
    buf[bufsize - 1] = '\0';
#ifdef HAVE_VSNPRINTF
    retcode = vsnprintf(buf, bufsize, format, ap);
#else
    retcode = vsprintf(buf, format, ap);
#endif
    if (retcode > 0 && (buf[bufsize - 1] != '\0' || retcode > bufsize - 1)) {
        syslog(LOG_ERR, "s_snprintf(..., %lu, ...) = %d: buffer overrun\n",
               (unsigned long) bufsize, retcode);
        s_abort();
    }
    va_end(ap);
    return retcode;
}


/*
**  read replacement that ignores EINTR.
*/
ssize_t
s_read(int fd, char *buf, size_t len)
{
    ssize_t code;

    do {
        code = read(fd, buf, len);
    } while (code < 0 && errno == EINTR);
    return code;
}
