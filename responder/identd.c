/*  $Id: identd.c 2429 2006-02-08 20:01:28Z rra $
**
**  A TCP/IP S/Ident protocol server (main routines).
**
**  Written by Peter Eriksson <pen@lysator.liu.se>, 1993-10-07
**  Extensively modified for S/Ident by Booker C. Bense <bbense@stanford.edu>
**
**  The pidentd portions of this program were originally under the following
**  license:  This program is in the public domain and may be used freely by
**  anyone who wants to.
*/

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

/* BSDI needs <netinet/in.h> before <arpa/inet.h>. */
#include <netinet/in.h>
#include <arpa/inet.h>

#if HAVE_SYS_SELECT_H
# include <sys/select.h>
#endif

#include "identd.h"

int verbose_flag = 0;
int debug_flag   = 0;
int syslog_flag  = 0;
int unknown_flag = 0;
int uidonly_flag = 0;
int users_can_nosident = 1;
int allow_trivial = 0;
int mutual_auth_flag = 0;
int client_only_flag = 0;

/*
**  Return the name of the connecting host, or the IP number as a string.
*/
char *
gethost(struct in_addr *addr)
{
    int i;
    struct hostent *hp;
    char *hostname;

    /* Make sure that forward and reverse DNS match to use a hostname. */
    hp = gethostbyaddr((char *) addr, sizeof(*addr), AF_INET);
    if (hp != NULL) {
        hostname = strdup(hp->h_name);
        if (hostname == NULL)
            return inet_ntoa(*addr);
        hp = gethostbyname(hostname);
        free(hostname);
        if (hp != NULL)
            for (i = 0; hp->h_addr_list[i]; i++)
                if (memcmp(hp->h_addr_list[i], addr, sizeof(*addr)) == 0)
                    return hp->h_name;
    }
    return inet_ntoa(*addr);
}


/*
**  Main entry point into this daemon.
*/
int
main(int argc, char *argv[])
{
    socklen_t len;
    struct sockaddr_in sin;
    struct in_addr laddr, faddr;
    int option;

    /* Parse options. */
    opterr = 0;
    while ((option = getopt(argc, argv, "CdeMNnlTVv")) != EOF) {
        switch (option) {
        case 'C':
            client_only_flag++;
            break;
        case 'd':
            debug_flag++;
            break;
        case 'e':
            unknown_flag = 1;
            break;
        case 'l':
            syslog_flag++;
            break;
        case 'M':
            mutual_auth_flag++;
            break;
        case 'N':
            users_can_nosident = 0;
            break;
        case 'n':
            uidonly_flag = 1;
            break;
        case 'T':
            allow_trivial++ ;
            break;
        case 'v':
            verbose_flag++;
            break;
        case 'V':
            printf("%s\n", PACKAGE_STRING);
            exit(0);
            break;
        default:
            fprintf(stderr, "sidentd: bad option -%c\n", optopt);
            exit(1);
            break;
        }
    }
    argc -= optind;
    if (argc > 0) {
        fprintf(stderr, "sidentd: too many arguments\n");
        exit(1);
    }

    /* Get the remote Internet address.  If this fails, a user has tried to
       start us from the command line or the network link died; either way,
       the message won't reach the other end. */
    len = sizeof(sin);
    if (getpeername(0, (struct sockaddr *) &sin, &len) < 0) {
        fprintf(stderr, "sidentd: getpeername failed: %s", strerror(errno));
        exit(1);
    }
    faddr = sin.sin_addr;

    /* If we're logging, open the connection to the syslog daemon. */
    if (syslog_flag || verbose_flag)
        openlog("sidentd", LOG_PID, LOG_DAEMON);

    /* Get the local Internet address.  If this fails, exit, since we have to
       have it to check requests. */
    len = sizeof(sin);
    if (getsockname(0, (struct sockaddr *) &sin, &len) == -1) {
        fprintf(stderr, "sidentd: getsockname failed: %s", strerror(errno));
        exit(1);
    }
    laddr = sin.sin_addr;

    /* Parse the ident request. */
    parse(stdin, &laddr, &faddr);
    exit(0);
}
