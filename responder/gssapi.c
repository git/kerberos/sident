/*  $Id: gssapi.c 590 2005-09-23 01:48:18Z eagle $
**
**  GSS-API implementation for an S/Ident responder.
**
**  Written by Russ Allbery <rra@stanford.edu>
**
**  This file contains the responder portion of a GSS-API S/Ident protocol
**  implementation.  As written, it only supports Kerberos v5.  It supports
**  both the Heimdal and MIT Kerberos implementations.
*/

#include <ctype.h>
#include <dirent.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <syslog.h>
#include <unistd.h>

#include "identd.h"
#include "sident.h"

#ifdef HAVE_GSSAPI_H
# include <gssapi.h>
#else
# include <gssapi/gssapi.h>
# include <gssapi/gssapi_generic.h>
#endif

#ifndef HAVE_GSS_RFC_OIDS
# define GSS_C_NT_USER_NAME gss_nt_user_name
#endif

extern int do_error();
extern int do_out();
extern int ident_decodebase64();
extern void ident_writebase64();
extern int parse_auth_line();
extern char *sfgets();

/* The steps of the authentication exchange. */
enum gssapi_step {
    STEP_ERROR = -1,            /* An error has occurred. */
    STEP_CONTEXT,               /* Need to establish a context. */
    STEP_FINAL,                 /* Need to send the authenticator. */
    STEP_DONE                   /* Completed the exchange. */
};

/* Holds the current GSS-API authentication exchange state. */
struct gssapi_state {
    enum gssapi_step step;      /* Step of the authentication exchange. */
    char *service;              /* GSS-API ASCII name of requester. */
    gss_name_t service_name;    /* GSS-API internal name of requester. */
    gss_ctx_id_t context;       /* GSS-API context with requester. */
};


/*
**  Lowercase a string in place.
*/
static void
lowercase(char *string)
{
    char *p;

    for (p = string; *p != '\0'; p++)
        *p = tolower((unsigned char) *p);
}


/*
**  Show the GSS-API error message for a particular type of status.  prefix is
**  prepended to any printed error messages.
*/
static void
debug_show_status_type(const char *prefix, OM_uint32 code, int type)
{
    OM_uint32 min_stat;
    gss_buffer_desc message;
    OM_uint32 context;

    context = 0;
    while (1) {
        gss_display_status(&min_stat, code, type, GSS_C_NULL_OID, &context,
                           &message);
        syslog(LOG_DEBUG, "GSS-API error %s: %s", prefix,
               (char *) message.value);
        gss_release_buffer(&min_stat, &message);
        if (!context)
            break;
    }
}


/*
**  Show the GSS-API error message of a failure.  prefix is prepended to any
**  printed error messages, and the other two arguments should be the return
**  code and mechanism status of the failing GSS-API function, respectively.
*/
static void
debug_show_status(const char *prefix, OM_uint32 maj_stat, OM_uint32 min_stat)
{
    debug_show_status_type(prefix, maj_stat, GSS_C_GSS_CODE);
    debug_show_status_type(prefix, min_stat, GSS_C_MECH_CODE);
}


/*
**  Given a file, make sure it looks like a ticket.  Ideally we would open
**  the ticket cache and make sure that it's not expired, but that's a bit
**  more than I know how to do yet.  So just make some basic checks of
**  reasonableness.  Takes the file name and the UID whose ticket we're
**  looking for and returns true if it looks like a ticket.
*/
static int
gssapi_ticket_check(const char *filename, uid_t uid)
{
    struct stat st;

    if (lstat(filename, &st) < 0)
        return 0;
    if (!S_ISREG(st.st_mode))
        return 0;
    if (st.st_uid != uid)
        return 0;
    if ((st.st_mode & 07777) != (S_IRUSR | S_IWUSR))
        return 0;
    if (st.st_size < 100)
        return 0;
    return 1;
}


/*
**  Find a ticket cache for a given user.  If one is found, saves the path to
**  the ticket file in the KRB5CCNAME environment variable where it can be
**  picked up by the GSS-API library calls.  Returns IDENT_AUTH_OKAY on
**  success, otherwise an error code to pass along to the requester.
*/
static int
gssapi_ticket_find(struct ident_auth_data *auth_data)
{
    DIR *tickets;
    struct dirent *file;
    char filename[MAXPATHLEN];
    char *env;

    tickets = opendir("/tmp");
    if (tickets == NULL)
        return IDENT_SYSTEM_ERROR;
    for (file = readdir(tickets); file != NULL; file = readdir(tickets)) {
        if (strncmp(file->d_name, "krb5cc_", strlen("krb5cc_")) != 0)
            continue;
        if (strlen(file->d_name) > MAXPATHLEN - 1 - 4 - 1)
            continue;
        sprintf(filename, "/tmp/%s", file->d_name);
        if (gssapi_ticket_check(filename, auth_data->uid)) {
            env = malloc(strlen("KRB5CCNAME=") + strlen(filename) + 1);
            if (env == NULL)
                return IDENT_SYSTEM_ERROR;
            strcpy(env, "KRB5CCNAME=");
            strcat(env, filename);
            if (putenv(env) != 0)
                return IDENT_SYSTEM_ERROR;
            closedir(tickets);
            if (syslog_flag)
                syslog(LOG_INFO, "ticket file %s for %d", filename,
                       auth_data->uid);
            return IDENT_AUTH_OKAY;
        } else if (verbose_flag) {
            syslog(LOG_DEBUG, "Rejected %s", file->d_name);
        }
    }
    closedir(tickets);
    return IDENT_USER_CANT_AUTH;
}


/*
**  Start the responder side of the authentication exchange.  Returns an error
**  code or IDENT_AUTH_OKAY.
*/
int
gssapi_responder_start(struct ident_auth_data *auth_data)
{
    struct gssapi_state *gstate;
    struct hostent *hp;
    gss_buffer_desc token;
    OM_uint32 maj_stat, min_stat;
    int status;

    gstate = malloc(sizeof(struct gssapi_state));
    if (gstate == NULL)
        return IDENT_SYSTEM_ERROR;
    auth_data->auth_method_data = gstate;

    /* Initialize the state. */
    gstate->service = NULL;
    gstate->service_name = GSS_C_NO_NAME;
    gstate->step = STEP_CONTEXT;
    gstate->context = GSS_C_NO_CONTEXT;

    /* Find the ticket file here and then fill in its data. */
    status = gssapi_ticket_find(auth_data);
    if (status != IDENT_AUTH_OKAY)
        return status;

    /* Build the service name. */
    hp = gethostbyaddr((const char *) auth_data->remote_addr,
                       sizeof(*auth_data->remote_addr), AF_INET);
    if (hp == NULL)
        return IDENT_SYSTEM_ERROR;
    gstate->service = malloc(strlen("ident/") + strlen(hp->h_name) + 1);
    if (gstate->service == NULL)
        return IDENT_SYSTEM_ERROR;
    strcpy(gstate->service, "ident/");
    strcat(gstate->service, hp->h_name);
    lowercase(gstate->service);

    /* Establish the internal name. */
    token.value = gstate->service;
    token.length = strlen(gstate->service) + 1;
    maj_stat = gss_import_name(&min_stat, &token, GSS_C_NT_USER_NAME,
                               &gstate->service_name);
    if (maj_stat != GSS_S_COMPLETE) {
        if (verbose_flag)
            debug_show_status("import_name", maj_stat, min_stat);
        return IDENT_SYSTEM_ERROR;
    }

    /* This is all we can do until we can start talking to the network. */
    return IDENT_AUTH_OKAY;
}


/*
**  Send a single token to the requester, base64-encoding it.  Returns an
**  error code or IDENT_AUTH_OKAY.
*/
static int
gssapi_send_token(struct ident_auth_data *auth_data, gss_buffer_t token)
{
    char output[IDENT_BUFFER_SIZE];
    char line[IDENT_BUFFER_SIZE];

    ident_writebase64(token->value, output, token->length);
    sprintf(line, " AUTHENTICATE : %s , %s ",
            auth_data->auth_struct->auth_method, output);
    do_out(line, auth_data);
    return IDENT_AUTH_OKAY;
}


/*
**  Receive a token from the requester.  Puts the decoded token into token and
**  returns an error code or IDENT_AUTH_OKAY.
*/
static int
gssapi_recv_token(struct ident_auth_data *auth_data, gss_buffer_t token)
{
    char line[IDENT_BUFFER_SIZE], data[IDENT_BUFFER_SIZE];
    char *result;
    int status;
    struct ident_auth_data reply;

    line[0] = '\0';
    result = sfgets(line, IDENT_BUFFER_SIZE, auth_data->fp,
                    IDENT_READ_TIMEOUT, auth_data);
    if (result == NULL)
        return IDENT_INVALID_REQ_INFO;
    status = parse_auth_line(line, &reply);
    if (status != IDENT_AUTH_OKAY)
        do_error(status, &reply);

    /* Verify that the relevant bits are still the same. */
    if ((reply.local_port != auth_data->local_port)
        || (reply.remote_port != auth_data->remote_port)
        || strcmp(reply.auth_struct->auth_method,
                  auth_data->auth_struct->auth_method) != 0) {
        if (verbose_flag)
            syslog(LOG_DEBUG,
                   "Auth reply does not match auth data: %d,%d,%s %d,%d,%s",
                   reply.local_port, reply.remote_port,
                   reply.auth_struct->auth_method,
                   auth_data->local_port, auth_data->remote_port,
                   auth_data->auth_struct->auth_method);
        return IDENT_INVALID_REQ_INFO;
    }

    /* Extract the data. */
    sscanf(reply.authenticate_data, " GSSAPI , %[A-Za-z0-9+/=]", data);
    status = ident_decodebase64(data);
    if (status < 4)
        return IDENT_INVALID_REQ_INFO;

    /* Put it into the token. */
    token->length = status;
    token->value = malloc(status);
    if (token->value == NULL)
        return IDENT_SYSTEM_ERROR;
    memcpy(token->value, data, status);
    return IDENT_AUTH_OKAY;
}


/*
**  Establish a GSS-API context with the requester.  Returns an error code or
**  IDENT_AUTH_OKAY.
*/
static int
gssapi_responder_context(struct ident_auth_data *auth_data)
{
    struct gssapi_state *gstate = auth_data->auth_method_data;
    gss_buffer_desc *token = GSS_C_NO_BUFFER;
    gss_buffer_desc send_token, recv_token;
    OM_uint32 maj_stat, min_stat, sec_stat, flags;
    int status;

    /* GSS-API negotiation may take multiple rounds, so repeat until it is
       complete. */
    do {
        /* Perform the next step of the GSS-API negotiation. */
        maj_stat = gss_init_sec_context(&sec_stat,
                                        GSS_C_NO_CREDENTIAL,
                                        &gstate->context,
                                        gstate->service_name,
                                        GSS_C_NULL_OID,
                                        GSS_C_MUTUAL_FLAG | GSS_C_REPLAY_FLAG,
                                        0,
                                        NULL, /* no channel bindings */
                                        token,
                                        NULL, /* ignore mechanism type */
                                        &send_token,
                                        &flags,
                                        NULL); /* ignore time_rec */
        if (token != GSS_C_NO_BUFFER)
            gss_release_buffer(&min_stat, &recv_token);

        /* Send the resulting token to the other side.  This token may be
           empty if we're in the last round. */
        if (verbose_flag)
            syslog(LOG_DEBUG, "Sending init_sec_context token (size=%d)",
                   send_token.length);
        status = gssapi_send_token(auth_data, &send_token);
        if (status < 0)
            goto fail;

        /* If the status returned by the GSS-API call is abnormal, abort. */
        if (maj_stat != GSS_S_COMPLETE && maj_stat != GSS_S_CONTINUE_NEEDED) {
            if (verbose_flag)
                debug_show_status("init_sec_context", maj_stat, sec_stat);
            goto fail;
        }

        /* If we have to go another round, receive the next token from the
           other side. */
        if (maj_stat == GSS_S_CONTINUE_NEEDED) {
            if (verbose_flag)
                syslog(LOG_DEBUG, "Continue needed");
            status = gssapi_recv_token(auth_data, &recv_token);
            if (status < 0)
                goto fail;
            token = &recv_token;
        }
        gss_release_buffer(&min_stat, &send_token);
    } while (maj_stat == GSS_S_CONTINUE_NEEDED);

    /* Completed successfully. */
    return IDENT_AUTH_OKAY;

 fail:
    gss_release_buffer(&min_stat, &send_token);
    if (gstate->context != GSS_C_NO_CONTEXT)
        gss_delete_sec_context(&min_stat, &gstate->context, GSS_C_NO_BUFFER);
    return (status != 0) ? status : IDENT_AUTH_FAIL;
}


/*
**  Builds an S/Ident authenticator from the authentication data.  The
**  authenticator is stored in newly allocated memory, with a pointer to the
**  authenticator returned in auth and its length returned in auth_len.
**  Returns true on success, false otherwise.
*/
static int
sident_build_authenticator(struct ident_auth_data *auth_data,
                           size_t *auth_len, char **auth)
{
    IDENT_INT16 value, userlen;
    struct passwd *pwd;
    size_t length, pad;

    /* Get the local username so that we know the authenticator length. */
    pwd = getpwuid(auth_data->uid);
    if (pwd == NULL)
        return 0;

    /* Allocate space for the authenticator. */
    userlen = strlen(pwd->pw_name);
    length = 2 + 2 + 2 + 2 + userlen;
    pad = 8 - (length % 8);
    if (pad == 8)
        pad = 0;
    length += pad;
    *auth = malloc(length);
    if (*auth == NULL)
        return 0;
    *auth_len = length;

    /* Now copy in the data.  First an all-zero flag bit (since the mutual
       authentication flag is irrelevant for GSS-API), then the responder port
       and requester port, then the length of the username and the username,
       then all-zero padding. */
    memset(*auth, 0, 2);
    value = htons(auth_data->local_port);
    memcpy(*auth + 2, &value, 2);
    value = htons(auth_data->remote_port);
    memcpy(*auth + 4, &value, 2);
    value = htons(userlen);
    memcpy(*auth + 6, &value, 2);
    memcpy(*auth + 8, pwd->pw_name, userlen);
    memset(*auth + 8 + userlen, 0, pad);
    if (verbose_flag)
        syslog(LOG_DEBUG, "Built token: %d %d %d %s", auth_data->local_port,
               auth_data->remote_port, userlen, pwd->pw_name);

    /* That's all there is to it. */
    return 1;
}


/*
**  Send the S/Ident authenticator.  This is done at the conclusion of the
**  authentication exchange.  This function accepts a token from the requester
**  and returns the S/Ident authenticator.  This concludes the GSS-API S/Ident
**  exchange.
*/
static int
gssapi_responder_sendauth(struct ident_auth_data *auth_data)
{
    struct gssapi_state *gstate = auth_data->auth_method_data;
    gss_buffer_desc send_token, recv_token, token;
    int conf_state, status;
    OM_uint32 maj_stat, min_stat, flags;
    size_t auth_len;
    char *auth;

    /* Receive configuration information from requester. */
    status = gssapi_recv_token(auth_data, &recv_token);
    if (status != IDENT_AUTH_OKAY)
        return status;

    /* Unwrap the token.  The first octet of the data is the supported
       security layers and the second through the fourth constitute the
       maximum message length accepted.  Ignore this for now, although do
       check the unwrapping. */
    maj_stat = gss_unwrap(&min_stat, gstate->context, &recv_token, &token,
                          &conf_state, NULL);
    gss_release_buffer(&min_stat, &recv_token);
    if (maj_stat != GSS_S_COMPLETE) {
        if (verbose_flag)
            debug_show_status("gss_unwrap", maj_stat, min_stat);
        return IDENT_INVALID_REQ_INFO;
    }
    gss_release_buffer(&min_stat, &token);

    /* Our response consists of an octet indicating the requested security
       layer and then three octets indicating the maximum message we can
       receive, followed by the authenticator.  Pick 2048 for the length, very
       arbitrarily. */
    if (!sident_build_authenticator(auth_data, &auth_len, &auth))
        return IDENT_SYSTEM_ERROR;
    token.length = auth_len + 4;
    token.value = malloc(token.length + 4);
    if (token.value == NULL)
        return IDENT_SYSTEM_ERROR;
    flags = ntohl(1024);
    memcpy(token.value, &flags, 4);
    memcpy((char *) token.value + 4, auth, auth_len);
    free(auth);

    /* Wrap the response and send it. */
    maj_stat = gss_wrap(&min_stat, gstate->context, 0, 0, &token, NULL,
                        &send_token);
    gss_release_buffer(&min_stat, &token);
    if (maj_stat != GSS_S_COMPLETE) {
        if (verbose_flag)
            debug_show_status("gss_wrap", maj_stat, min_stat);
        return IDENT_SYSTEM_ERROR;
    }
    status = gssapi_send_token(auth_data, &send_token);
    gss_release_buffer(&min_stat, &send_token);
    return status;
}


/*
**  Perform the authentication exchange.  This is the high-level function that
**  calls the other functions specified in this file in order to complete an
**  authentication exchange with the remote system.
*/
int
gssapi_responder_auth(struct ident_auth_data *auth_data)
{
    struct gssapi_state *gstate = auth_data->auth_method_data;
    int status;

    while (gstate->step != STEP_ERROR) {
        switch (gstate->step) {
        case STEP_CONTEXT:
            status = gssapi_responder_context(auth_data);
            if (status != IDENT_AUTH_OKAY) {
                gstate->step = STEP_ERROR;
                return status;
            }
            gstate->step = STEP_FINAL;
            break;
        case STEP_FINAL:
            status = gssapi_responder_sendauth(auth_data);
            if (status != IDENT_AUTH_OKAY) {
                gstate->step = STEP_ERROR;
                return status;
            }
            gstate->step = STEP_DONE;
            break;
        case STEP_DONE:
            return IDENT_AUTH_OKAY;
            break;
        default:
            gstate->step = STEP_ERROR;
            return IDENT_SYSTEM_ERROR;
            break;
        }
    }
    return IDENT_SYSTEM_ERROR;
}


/*
**  Free GSS-API-specific responder state.
*/
void
gssapi_free_state(void *state)
{
    struct gssapi_state *gstate = state;
    OM_uint32 min_stat;

    if (gstate->service != NULL)
        free(gstate->service);
    if (gstate->service_name != GSS_C_NO_NAME)
        gss_release_name(&min_stat, &gstate->service_name);
    if (gstate->context != GSS_C_NO_CONTEXT)
        gss_delete_sec_context(&min_stat, &gstate->context, GSS_C_NO_BUFFER);
    free(gstate);
}
