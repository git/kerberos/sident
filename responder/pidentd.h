/** @file
 * Provides necessary glue in order to use pidentd kernel files.
 *
 * This file provides the glue necessary to use pident 3.* k_os files with
 * S/Ident without any changes to the k_os files.
 *
 * $Id: pidentd.h 556 2004-06-18 08:23:34Z eagle $
 */

#ifndef PIDENTD_H
#define PIDENTD_H

#include <sys/types.h>
#include <netinet/in.h>

#ifndef SYS_SOCKET_H_INCLUDED
# define SYS_SOCKET_H_INCLUDED
# include <sys/socket.h>
#endif

#define NO_UID ((uid_t) -1)
#define NO_PID ((pid_t) -1)

/* Taken from pidentd's sockaddr.h, the non-IPv6 case.  Eventually we'll have
   to pull in the IPv6 pieces, but sident needs a bunch more work to support
   IPv6.  These are used by the Solaris modules. */
#define SGFAM(sag)      AF_INET
#define SGSIZE(sag)     sizeof(struct in_addr)
#define SGSOCKSIZE(sag) sizeof(struct sockaddr_in)
#define SGPORT(sag)     ((sag).sin_port)
#define SGADDRP(sag)    ((char *) &(sag).sin_addr)

/* Only used by ka_init, which we don't call. */
extern const char *osinfo_build;
char *osinfo_get(char *osinfo_current);

/* Various safe versions of regular functions used by the kernel modules. */
void *s_malloc(size_t);
void s_free(void *);
char *s_strdup(const char *);
int s_open(const char *, int, ...);
int s_snprintf(char *, size_t, const char *, ...);
ssize_t s_read(int, char *, size_t);

/* Used as the interface to the kernel routines.  This matches the kernel
   struct in pidentd 3.x except that we omit the av parameter, since none of
   the kernel modules actually use it (it's a thread mutex). */
struct kernel {
    struct sockaddr_in local;
    struct sockaddr_in remote;

    /* Result parameters */
    int status;
    uid_t euid;
    uid_t ruid;

    pid_t pid;
    char *cmd;
    char *argv;
};

/* Filled in by ka_init for some drivers.  We don't use them, but they have to
   be here to compile the kernel modules. */
extern int kernel_threads;
extern int kernel_buffers;
extern int kernel_attempts;

/* The kernel modules use debug instead of debug_flag. */
extern int debug_flag;
#define debug debug_flag

/* These interface functions we actually call in the sident code. */
extern struct kernel *kernel_alloc(void);
extern void kernel_free(struct kernel *kp);

/* The kernel modules provide the following three functions.  We never call
   ka_init, but we do use the other two. */
extern int ka_init(void);
extern int ka_open(void **misc);
extern int ka_lookup(void *misc, struct kernel *kp);

#endif /* PIDENTD_H */
