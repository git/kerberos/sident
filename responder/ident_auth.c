/*  $Id: ident_auth.c 2430 2006-02-08 21:32:45Z rra $
**
**  SASL-based authentication mechanisms for identd.
**
**  Written by Booker Bense <bbense@stanford.edu>, 1996-08-28
**
**  This file contains the core parsing routines for the S/Ident extensions as
**  well as the implementation of Kerberos authentication for Kerberos v4.
**  Much of this code was originally taken from acte_krb.c in the Cyrus IMAP
**  distribution.  At the time, the copyright and license statement was as
**  follows:
**
**      (C) Copyright 1994 by Carnegie Mellon University
**
**                      All Rights Reserved
**
**  Permission to use, copy, modify, distribute, and sell this software and
**  its documentation for any purpose is hereby granted without fee, provided
**  that the above copyright notice appear in all copies and that both that
**  copyright notice and this permission notice appear in supporting
**  documentation, and that the name of Carnegie Mellon University not be used
**  in advertising or publicity pertaining to distribution of the software
**  without specific, written prior permission.  Carnegie Mellon University
**  makes no representations about the suitability of this software for any
**  purpose.  It is provided "as is" without express or implied warranty.
**
**  CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
**  SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
**  IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE FOR ANY SPECIAL,
**  INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
**  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
**  OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
**  PERFORMANCE OF THIS SOFTWARE.
*/

#include "config.h"

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <syslog.h>

#ifdef HAVE_KRB4
# ifdef HAVE_KERBEROSIV_KRB_H
#  include <kerberosIV/krb.h>
# else
#  include <krb.h>
# endif
#endif

#include "identd.h"

/* Exported definition of responder authentication mechanism */
static int null_responder_start();
static int null_responder_auth();
static void null_free_state();
static int trivial_responder_start();
static int trivial_responder_auth();

#ifdef HAVE_KRB4
static int krb_responder_start();
static int krb_responder_auth();
static void krb_free_state();
#endif

extern int do_out();
extern int do_error();
extern int parse_auth_line();
extern char *sfgets();

/* GSS-API implementation. */
extern int gssapi_responder_start();
extern int gssapi_responder_auth();
extern void gssapi_free_state();

#ifdef HAVE_KRB4
struct ident_responder_auth krb_ident_auth = {
    "KERBEROS_V4",
    krb_responder_start,
    krb_responder_auth,
    krb_free_state
};
#endif

struct ident_responder_auth gssapi_ident_auth = {
    "GSSAPI",
    gssapi_responder_start,
    gssapi_responder_auth,
    gssapi_free_state
};

struct ident_responder_auth otp_ident_auth = {
    "OTP_RFC1938",
    null_responder_start,
    null_responder_auth,
    null_free_state
};

struct ident_responder_auth trivial_ident_auth = {
    "TRIVIAL",
    trivial_responder_start,
    trivial_responder_auth,
    null_free_state
};

#ifdef HAVE_KRB4
# define IDENT_METHODS_COUNT 4
#else
# define IDENT_METHODS_COUNT 3
#endif

/* TRIVIAL method must always be the last method */
#define IDENT_TRIVIAL_METHOD (IDENT_METHODS_COUNT - 1 )

struct ident_responder_auth *auth_methods[IDENT_METHODS_COUNT] = {
#ifdef HAVE_KRB4
    &krb_ident_auth,
#endif
    &gssapi_ident_auth,
    &otp_ident_auth,
    &trivial_ident_auth
};

/*
**  Given a line from a requester, find the requested auth type.  If the line
**  is NULL, assume the trivial (traditional) method.  Otherwise, find a
**  case-sensitive match for the authentication method given in the table of
**  acceptable authentication methods.  Set auth_struct to the struct for the
**  one found.  Returns an error code or IDENT_AUTH_OKAY.
*/
int
parse_auth(char *line, struct ident_responder_auth **auth_struct)
{
    int i, rcode;
    char auth_method[128];
    const char *method;

    *auth_struct = NULL;
    if (line == NULL) {
        *auth_struct = auth_methods[IDENT_TRIVIAL_METHOD];
        return IDENT_AUTH_OKAY;
    }

    /* Parse the line to find the authentication method. */
    rcode = sscanf(line, " %32[^ \t\n\r,]", auth_method);
    if (rcode != 1)
        return IDENT_INVALID_REQ_INFO;
    for (i = 0; i < IDENT_METHODS_COUNT; i++) {
        method = auth_methods[i]->auth_method;
        if (strncmp(auth_method, method, strlen(method)) == 0) {
            *auth_struct = auth_methods[i];
            return IDENT_AUTH_OKAY;
        }
    }
    return IDENT_INVALID_REQ_INFO;
}


/* Tables for encoding and decoding base64. */
static const char basis_64[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
#define XX 127
static const char index_64[256] = {
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,62, XX,XX,XX,63,
    52,53,54,55, 56,57,58,59, 60,61,XX,XX, XX,XX,XX,XX,
    XX, 0, 1, 2,  3, 4, 5, 6,  7, 8, 9,10, 11,12,13,14,
    15,16,17,18, 19,20,21,22, 23,24,25,XX, XX,XX,XX,XX,
    XX,26,27,28, 29,30,31,32, 33,34,35,36, 37,38,39,40,
    41,42,43,44, 45,46,47,48, 49,50,51,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
    XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX, XX,XX,XX,XX,
};
#define CHAR64(c)  (index_64[(unsigned char)(c)])

/*
**  Decode in-place a buffer encoded in base64.  Returns the length of the
**  decoded data or -1 on an error.
*/
int
ident_decodebase64(char *input)
{
    int len = 0;
    unsigned char *output = (unsigned char *)input;
    int c1, c2, c3, c4;

    while (*input) {
        c1 = *input++;
        if (CHAR64(c1) == XX)
            return -1;
        c2 = *input++;
        if (CHAR64(c2) == XX)
            return -1;
        c3 = *input++;
        if (c3 != '=' && CHAR64(c3) == XX)
            return -1;
        c4 = *input++;
        if (c4 != '=' && CHAR64(c4) == XX)
            return -1;
        *output++ = (CHAR64(c1) << 2) | (CHAR64(c2) >> 4);
        ++len;
        if (c3 == '=')
            break;
        *output++ = ((CHAR64(c2) << 4) & 0xf0) | (CHAR64(c3) >> 2);
        ++len;
        if (c4 == '=')
            break;
        *output++ = ((CHAR64(c3) << 6) & 0xc0) | CHAR64(c4);
        ++len;
    }

    return len;
}

/*
**  Encode data into base64.  Takes the input buffer, the output buffer, and
**  the length of the data in the input buffer.  The output buffer must be
**  long enough to hold the encoded input.
*/
void
ident_writebase64(char *input, char *output, int len)
{
    char buf[IDENT_BUFFER_SIZE];
    char *outptr;
    int buflen = 0;
    int c1, c2, c3;

    outptr = output;
    while (len > 0) {
        if (buflen >= sizeof(buf) - 4) {
            strncpy(outptr, buf, buflen);
            buflen = 0;
            outptr += buflen;
        }

        c1 = (unsigned char) *input++;
        buf[buflen++] = basis_64[c1 >> 2];

        if (--len == 0)
            c2 = 0;
        else
            c2 = (unsigned char) *input++;
        buf[buflen++] = basis_64[((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4)];

        if (len == 0) {
            buf[buflen++] = '=';
            buf[buflen++] = '=';
            break;
        }

        if (--len == 0)
            c3 = 0;
        else
            c3 = (unsigned char) *input++;
        buf[buflen++] = basis_64[((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6)];

        if (len == 0) {
            buf[buflen++] = '=';
            break;
        }

        --len;
        buf[buflen++] = basis_64[c3 & 0x3F];
    }

    if (buflen >= sizeof(buf) - 2) {
        strncpy(outptr, buf, buflen);
        outptr += buflen;
        buflen = 0;
    }
    buf[buflen++] = '\0';
    strcpy(outptr, buf);
}

/* Skip all the KERBEROS_V4 implementation unless we're building with v4
   support. */

#ifdef HAVE_KRB4

/* Private state used by this mechanism */
struct krb_state {
    char service[MAX_K_NAME_SZ + 1];
    int authstepno;
    des_cblock session;         /* Our session key */
    des_key_schedule schedule;  /* Schedule for our session key */
    int32_t challenge;

    /* Used by client. */
    char instance[INST_SZ];
    char realm[REALM_SZ];
};


/*
**  Free the space used by the opaque mechanism state pointer.
*/
static void
krb_free_state(void *state)
{
    memset(state, 0, sizeof(struct krb_state));
    free(state);
}


/*
**  Check whether a file is a valid ticket for our user of interest.  Returns
**  a Kerberos status code (KSUCCESS on success).
*/
static int
krb_is_ticket(char *tf_name, struct ident_auth_data *auth_data)
{
    struct stat stat_buf;
    int code;
    char pname[ANAME_SZ];
    char pinst[INST_SZ];
    CREDENTIALS cred;
    time_t now;

    /* Can we even stat the file? */
    if (lstat(tf_name, &stat_buf) < 0) {
        if (verbose_flag)
            syslog(LOG_DEBUG, "lstat error %d on %s", errno, tf_name);
        return (errno == ENOENT) ? NO_TKT_FIL : TKT_FIL_ACC;
    }

    /* Check ownership */
    if (stat_buf.st_uid != auth_data->uid ) {
        if (verbose_flag)
            syslog(LOG_DEBUG, "UID mismatch %d : %d on %s", stat_buf.st_uid,
                   auth_data->uid, tf_name);
        return TKT_FIL_ACC;
    }

    /* Check file perms for 600. */
    if ((stat_buf.st_mode & S_IRUSR) != S_IRUSR) {
        if (verbose_flag)
            syslog(LOG_DEBUG, "User can't read %s", tf_name);
        return TKT_FIL_ACC;
    }
    if (   ((stat_buf.st_mode & S_IRGRP) == S_IRGRP)
        || ((stat_buf.st_mode & S_IWGRP) == S_IWGRP)
        || ((stat_buf.st_mode & S_IROTH) == S_IROTH)
        || ((stat_buf.st_mode & S_IWOTH) == S_IWOTH)) {
        if (verbose_flag)
            syslog(LOG_DEBUG, "Incorrect file permissions on %s : %o",
                   tf_name, stat_buf.st_mode);
        return TKT_FIL_ACC ;
    }

    /* Okay now we should have a valid ticket file.  Let's look.  The
       following was mostly taken directly from klist.c in Kerberos v4. */

    /* Open ticket file */
    code =  tf_init(tf_name, R_TKT_FIL);
    if (code != KSUCCESS) {
        tf_close();
        if (verbose_flag)
            syslog(LOG_DEBUG, "tf_init failed %s", tf_name);
        return code;
    }

    /* Get principal name and instance. */
    if ((code = tf_get_pname(pname)) || (code = tf_get_pinst(pinst))) {
        tf_close();
        return code;
    }

    /* Ensure ticket is valid. */
    now = time(NULL);
    while (tf_get_cred(&cred) == KSUCCESS) {
        if (strcmp(cred.service, "krbtgt") != 0)
            continue;
        cred.issue_date = krb_life_to_time(cred.issue_date, cred.lifetime);
        if (now - cred.issue_date > 1) {
            tf_close();
            return KFAILURE;
        }
    }
    tf_close();
    if (syslog_flag)
        syslog(LOG_INFO, "ticket file %s: %s.%s for %d", tf_name, pname,
               pinst, auth_data->uid);

    return KSUCCESS;
}

/*
**  Find a valid ticket for the user in /tmp.  tktstring will have the path to
**  the ticket file filled in and must point to a buffer at least MAXPATHLEN
**  long.  Returns an error code or IDENT_AUTH_OKAY.
*/
static int
krb_find_ticket(char *tktstring, struct ident_auth_data *auth_data)
{
    int rcode;
    char temp[MAXPATHLEN];
    DIR *temp_dir;
    struct dirent *dp;

    /* Ideally, we want to dive into the kernel and get TKTFILE from the
       environment of the user's process.  Right now, we just cheat and look
       in typical places.  Start by trying the K4 standard. */
    sprintf(tktstring, "/tmp/tkt%d", auth_data->uid);
    if (krb_is_ticket(tktstring, auth_data) == KSUCCESS)
        return IDENT_AUTH_OKAY;

    /* Now we will try grupping through /tmp for a ticket file. */
    temp_dir = opendir("/tmp");
    for (dp = readdir(temp_dir); dp != NULL; dp = readdir(temp_dir)) {
        if (strncmp(dp->d_name, "tkt", strlen("tkt")) != 0)
            continue;
        if (strlen(dp->d_name) >= MAXPATHLEN - 6)
            continue;
        sprintf(temp, "/tmp/%s", dp->d_name);
        rcode = krb_is_ticket(temp, auth_data);
        if (rcode == KSUCCESS ) {
            strncpy(tktstring, temp, MAXPATHLEN - 1);
            closedir(temp_dir);
            return IDENT_AUTH_OKAY;
        }
        if (verbose_flag)
            syslog(LOG_DEBUG, "Rejected %s : %d", dp->d_name, rcode);
    }
    closedir(temp_dir);
    return IDENT_USER_CANT_AUTH;
}


/*
**  Start the responder side of an authentication exchange.  This function
**  doesn't send any data to the remote system.  It just makes sure that we
**  have a ticket file for the user and obtains the needed service ticket that
**  we'll use later on.  Returns an error code or IDENT_AUTH_OKAY.
*/
static int
krb_responder_start(struct ident_auth_data *auth_data)
{
    struct hostent *host_name;
    char *host;
    char instance[INST_SZ];
    char realm[REALM_SZ];
    char tktstring[MAXPATHLEN];
    KTEXT_ST authent;
    CREDENTIALS cr;
    struct krb_state *kstate;
    char *p;

    kstate = malloc(sizeof(struct krb_state));
    if (kstate == NULL)
        return IDENT_SYSTEM_ERROR;
    if (krb_find_ticket(tktstring, auth_data) != IDENT_AUTH_OKAY)
        return IDENT_USER_CANT_AUTH;
    krb_set_tkt_string(tktstring);

    /* Get the name of the remote host to determine the service ticket. */
    host_name = gethostbyaddr((char *) auth_data->remote_addr,
                              sizeof(struct in_addr), AF_INET);
    if (host_name == NULL)
        return IDENT_SYSTEM_ERROR;
    host = strdup(host_name->h_name);
    if (host == NULL)
        return IDENT_SYSTEM_ERROR;

    /* Form the name of the service ticket. */
    strncpy(instance, host_name->h_name, sizeof(instance) - 1);
    instance[sizeof(instance) - 1] = '\0';
    p = strchr(instance, '.');
    if (p != NULL)
        *p = '\0';
    for (p = instance; *p != '\0'; p++)
        if (isupper((int) *p))
            *p = tolower((int) *p);
    strncpy(realm, krb_realmofhost(host), REALM_SZ - 1);
    free(host);
    if (verbose_flag)
        syslog(LOG_DEBUG, "Fetching ticket :%s.%s@%s:",
               IDENT_AUTH_KRB_SERVICE, instance, realm);

    /* Fetch the service ticket and initialize our state information. */
    krb_mk_req(&authent, IDENT_AUTH_KRB_SERVICE, instance, realm, 0);
    memset(&authent, 0, sizeof(authent));
    if (krb_get_cred(IDENT_AUTH_KRB_SERVICE, instance, realm, &cr))
        return IDENT_USER_CANT_AUTH;
    memset(kstate, 0, sizeof(*kstate));
    strcpy(kstate->service, IDENT_AUTH_KRB_SERVICE);
    kstate->authstepno = 0;
    memcpy(kstate->session, cr.session, sizeof(des_cblock));
    des_key_sched(kstate->session, kstate->schedule);
    strcpy(kstate->instance, instance);
    strcpy(kstate->realm, realm);
    memset(&cr, 0, sizeof(cr));
    auth_data->auth_method_data = kstate;

    return IDENT_AUTH_OKAY;
}


/*
**  Perform responder-side authentication protocol exchange for Kerberos v4.
**  It does mutual authentication if needed and then returns the status of the
**  authentication.
*/
static int
krb_responder_auth(struct ident_auth_data *auth_data)
{
    static KTEXT_ST authent;
    struct krb_state *kstate = auth_data->auth_method_data;
    char tmp[IDENT_BUFFER_SIZE];
    char resp[IDENT_BUFFER_SIZE];
    char output[IDENT_BUFFER_SIZE];
    char *resp_ptr ;
    char line[IDENT_BUFFER_SIZE];
    char *line_ptr ;
    IDENT_INT16 i, j, k, resp_len, len, munch;
    int32_t challenge;
    struct ident_auth_data auth_reply;
    int code, rcode;
    int timeout = IDENT_READ_TIMEOUT;
    struct passwd *paswd_entry;

    while (kstate->authstepno > -1) {
        switch (kstate->authstepno++) {
        case 0:
            /* Get challenge. */
            sscanf(auth_data->authenticate_data,
                   " KERBEROS_V4 , %[A-Za-z0-9+/=]", tmp);
            rcode = ident_decodebase64(tmp);

            /* Requester gave us challenge, respond with ticket and
               authenticator. */
            if (rcode < 4) {
                kstate->authstepno = -1;
                return IDENT_INVALID_REQ_INFO;
            }
            memcpy(&kstate->challenge, tmp, sizeof(int32_t));
            kstate->challenge = ntohl(kstate->challenge);
            code = krb_mk_req(&authent, kstate->service, kstate->instance,
                              kstate->realm, kstate->challenge);
            if (code != 0) {
                kstate->authstepno = -1;
                return IDENT_AUTH_FAIL;
            }

            /* Now we need to formulate the response.  The response should
               look like the following:

                   <port>, <port> : AUTHENTICATE : KERBEROS_V4, <auth> <ident>

               <auth> is the 32-bit length of the authenticator in network
               byte order prepended to the authenticator and then encoded in
               base64.  <ident> is the 32-bit length of the ident information
               in network byte order prepended to the ident information and
               then encoded in base64.

               The ident information looks like:

                   [flag][resp port][req port][authid len][authid][pad]
                   <-16-><----16---><---16---><----16----><--??--><-?->

               where padding is added to the end to pad the whole thing to a
               multiple of eight.  The entire thing is then encrypted in the
               session key from the authenticator.  All values above are in
               network byte order. */
            munch = sizeof(IDENT_INT16);
            resp_ptr = resp;
            k = htons(mutual_auth_flag);
            memcpy(resp_ptr, &k, munch);
            resp_ptr += munch;
            k = htons(auth_data->local_port);
            memcpy(resp_ptr, &k, munch);
            resp_ptr += munch;
            k = htons(auth_data->remote_port);
            memcpy(resp_ptr, &k, munch);
            resp_ptr += munch;
            paswd_entry = getpwuid(auth_data->uid);
            i = strlen(paswd_entry->pw_name);
            k = htons(i);
            memcpy(resp_ptr, &k, munch);
            resp_ptr += munch;
            strcpy(resp_ptr, paswd_entry->pw_name);
            resp_ptr += i;
            k = sizeof(des_cblock);
            j = i % k;

            /* Pad to a multiple of sizeof(des_cblock). */
            memset(resp_ptr, 0, k - j);
            resp_len = 4 * (munch) + i + k - j;

            /* Log what we're doing for debugging purposes. */
            if (verbose_flag) {
                syslog(LOG_DEBUG, "Data: %4.4x:%4.4x:%4.4x:%4.4x:%s",
                       htons(mutual_auth_flag),
                       htons(auth_data->remote_port),
                       htons(auth_data->local_port),
                       htons(i), paswd_entry->pw_name);
                syslog(LOG_DEBUG, "Resp: %d:%4.4x:%4.4x:%4.4x:%4.4x:%s",
                       munch, *((IDENT_INT16 *) resp),
                       *((IDENT_INT16 *) &(resp[munch])),
                       *((IDENT_INT16 *) &(resp[2 * munch])),
                       *((IDENT_INT16 *) &(resp[3 * munch])),
                       &(resp[4 * munch]));
            }

            /* Encrypt the ident information. */
            des_pcbc_encrypt((des_cblock *) resp, (des_cblock *) output,
                             resp_len, kstate->schedule, &kstate->session, 1);

            /* Now, build the authenticator and convert it to base64. */
            k = htons(authent.length);
            memcpy(resp, &k, munch);
            resp_ptr = &resp[munch];
            memcpy(resp_ptr, authent.dat, authent.length);
            resp_ptr = &resp[munch + authent.length];
            k = htons(resp_len);
            memcpy(resp_ptr, &k, munch);
            resp_ptr = &resp[munch + authent.length + munch];
            memcpy(resp_ptr, output, resp_len);
            len = munch + authent.length + munch + resp_len;
            ident_writebase64(resp, tmp, len);

            /* Send the output to the remote side.  If we're not doing mutual
               authentication, we're done. */
            sprintf(line, " AUTHENTICATE : %s , %s ",
                    auth_data->auth_struct->auth_method, tmp);
            do_out(line, auth_data);
            if (mutual_auth_flag & MUTUAL_AUTH_BIT)
                return IDENT_AUTH_OKAY;
            break;

        case 1:
            /* Requester gave us mutual auth reply. */
            line[0] = '\0';
            line_ptr = sfgets(line, IDENT_BUFFER_SIZE, auth_data->fp,
                              timeout, auth_data);
            if (line_ptr == NULL)
                do_error(IDENT_NO_MUTUAL_AUTH, auth_data);
            rcode = parse_auth_line(line, &auth_reply);
            if (rcode != IDENT_AUTH_OKAY)
                do_error(rcode, &auth_reply);

            /* Check if the data is the same */
            if (   (auth_reply.local_port != auth_data->local_port)
                || (auth_reply.remote_port != auth_data->remote_port)
                || (strncmp(auth_reply.auth_struct->auth_method,
                            "KERBEROS_V4", strlen("KERBEROS_V4")) != 0)) {
                if (verbose_flag)
                    syslog(LOG_DEBUG, "Auth reply does not match auth_data"
                           " %d,%d : %d,%d", auth_reply.local_port,
                           auth_reply.remote_port, auth_data->local_port,
                           auth_data->remote_port);
                do_error(IDENT_MUTUAL_AUTH_FAIL, auth_data);
            }

            /* Get challenge */
            sscanf(auth_reply.authenticate_data,
                   " KERBEROS_V4 , %[A-Za-z0-9+/=]", tmp);
            rcode = ident_decodebase64(tmp);
            if (rcode < 4) {
                kstate->authstepno = -1;
                return IDENT_INVALID_RESP_INFO;
            }

            /* Decrypt the challenge and check it. */
            des_ecb_encrypt((des_cblock *) tmp, (des_cblock *) tmp,
                            kstate->schedule, 0);
            memcpy(&challenge, tmp, sizeof(challenge));
            challenge = ntohl(challenge);
            if (challenge != kstate->challenge + 1) {
                kstate->authstepno = -1;
                if (verbose_flag)
                    syslog(LOG_DEBUG, "Challenge failed: %d:%d",
                           kstate->challenge, challenge);
                return IDENT_MUTUAL_AUTH_FAIL;
            }
            if (verbose_flag)
                syslog(LOG_DEBUG, "Challenge succeeded: %d:%d",
                       kstate->challenge, challenge);
            return IDENT_AUTH_OKAY;

        default:
            kstate->authstepno = -1;
            return IDENT_SYSTEM_ERROR;
        }
    }
    return IDENT_AUTH_FAIL;
}

#endif /* HAVE_KRB4 */

/*
**  Start traditional ident authentication.  Does nothing except retrieve the
**  password file entry for the user UID.  Returns an error code or
**  IDENT_AUTH_OKAY.
*/
static int
trivial_responder_start(struct ident_auth_data *auth_data)
{
    struct passwd *paswd_entry;

    if (allow_trivial) {
        paswd_entry = getpwuid(auth_data->uid);
        if (!paswd_entry || auth_data->uid != paswd_entry->pw_uid)
            do_error(IDENT_NO_USER, auth_data);
        auth_data->auth_method_data = paswd_entry;
        return IDENT_AUTH_OKAY;
    } else
        do_error(IDENT_INVALID_REQ_INFO, auth_data);
    return IDENT_AUTH_FAIL;
}


/*
**  Send a traditional ident response, either the username or the UID
**  depending on our flags if trivial authentication is enabled.  If it's not,
**  returns the appropriate error message.  Returns an error code or
**  IDENT_AUTH_OKAY.
*/
static int
trivial_responder_auth(struct ident_auth_data *auth_data)
{
    struct passwd *pwd_entry;
    char tmp[IDENT_BUFFER_SIZE];

    /* Do we want to do noident check here? */
    if (allow_trivial) {
        pwd_entry = auth_data->auth_method_data;
        if (uidonly_flag)
            sprintf(tmp, " USERID : UNIX :%d ", auth_data->uid);
        else
            sprintf(tmp, " USERID : UNIX :%s ", pwd_entry->pw_name);
        do_out(tmp, auth_data);
        return IDENT_AUTH_OKAY;
    } else
        do_error(IDENT_INVALID_REQ_INFO, auth_data);
    return IDENT_AUTH_FAIL;
}

/* Null responder implementations, which always return an error. */
static int null_responder_start(void) { return IDENT_AUTH_NOT_SUPPORTED; }
static int null_responder_auth(void)  { return IDENT_AUTH_NOT_SUPPORTED; }
static void null_free_state(void)     { return; }
