/*  $Id: parse.c 590 2005-09-23 01:48:18Z eagle $
**
**  The S/Ident protocol parser.
**
**  Written by Peter Eriksson <pen@lysator.liu.se>
**  Modifications for S/Ident by Booker C. Bense <bbense@stanford.edu>
**
**  Here's where the core parsing routines for the ident protocol are.  The
**  functions here dispatch to the appropriate method-specific functions to do
**  the authentication work, but the kernel lookup is done here.
*/

#include <errno.h>
#include <pwd.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

/* BSDI needs <netinet/in.h> before <arpa/inet.h>. */
#include <netinet/in.h>
#include <arpa/inet.h>

#include "identd.h"
#include "sident.h"
#include "pidentd.h"

/* The number of times to retry a kernel lookup before failing. */
#define MAX_RETRY 20

extern int do_error();
extern int parse_auth();


/*
**  Check for the existence of a ".nosident" file in a user's home directory.
**  Return whether one was found.
*/
static int
check_nosident(uid_t uid)
{
    char *tmp_path, *homedir;
    struct stat sbuf;
    int rcode;
    struct passwd *pw;

    if (users_can_nosident) {
        pw = getpwuid(uid);
        if (pw == NULL)
            return 0;
        homedir = pw->pw_dir;
        if (homedir == NULL)
            return 0;
        tmp_path = malloc(strlen(homedir) + sizeof("/.nosident") + 1);
        if (tmp_path == NULL)
            return 0;
        strcpy(tmp_path, homedir);
        strcat(tmp_path, "/.nosident");
        rcode = stat(tmp_path, &sbuf);
        free(tmp_path);
        return (rcode == 0);
    } else
        return 0;
}

/* Timeout handling for sfgets. */
static jmp_buf context_read_timeout;
static void
readtimeout(int sig)
{
    longjmp(context_read_timeout, 1);
}


/*
**  An implementation of fgets that ignores EINTR interrupts and implements a
**  timeout.  Takes a buffer, the size of the buffer, the file to read from,
**  the timeout in seconds, and the authentication we're trying to perform
**  (for error reporting).  Returns NULL on error and a pointer to the buffer
**  otherwise.
**
**  This function needs to be rewritten to use select instead of setjmp and
**  alarm.  Not only will that be easier to read, it will also be more
**  reliable and understandable.
*/
char *
sfgets(char *buf, int siz, FILE *fp, time_t timeout,
       struct ident_auth_data *auth_data)
{
    char *p;

    if (fp == NULL) {
        buf[0] = '\0';
        return NULL;
    }

    /* Set the timeout. */
    if (timeout != 0) {
        if (signal(SIGALRM, readtimeout) == SIG_ERR)
            do_error(IDENT_SYSTEM_ERROR, auth_data);
        if (setjmp(context_read_timeout) != 0) {
            syslog(LOG_NOTICE, "timeout waiting for input");
            errno = 0;
            buf[0] = '\0';
            return NULL;
        }
        alarm(timeout);
    }

    /* Try to read. */
    p = NULL;
    buf[0] = '\0';
    while (!feof(fp) && !ferror(fp)) {
        errno = 0;
        p = fgets(buf, siz, fp);
        if (p != NULL || errno != EINTR)
            break;
        clearerr(fp);
    }
    alarm(0);
    if (verbose_flag)
        syslog(LOG_DEBUG, "In function sfgets :%d:%s:%s", siz, buf, p);
    return buf;
}


/*
**  Parse an incoming line.  If it contains authentication information, run
**  parse_auth to handle it.  Sets various things in the auth_data struct.
*/
int
parse_auth_line(char *line, struct ident_auth_data *auth_data)
{
    char arg[33];
    int rcode;
    char *p;

    p = strtok(line, ":");
    if (verbose_flag)
        syslog(LOG_DEBUG, "parse_auth_line: %s: port_data %s", line, p);
    if (p != NULL)
        auth_data->port_data = p;
    else
        return IDENT_INVALID_PORT;

    p = strtok(NULL, ":");
    auth_data->cmd_data = p;

    p = strtok(NULL, ":");
    auth_data->authenticate_data = p;

    /* Parse the port data into numbers. */
    rcode = sscanf(auth_data->port_data, " %d , %d ", &auth_data->local_port,
                   &auth_data->remote_port);
    if (rcode < 2)
        return IDENT_INVALID_REQ_INFO;

    /* If we have a command, parse it into a variable and then see if it's an
       AUTHENTICATE commad.  If not, it's a trivial auth request. */
    if (auth_data->cmd_data != NULL)
        rcode = sscanf(auth_data->cmd_data, " %32s ", arg);
    else
        rcode = 0;
    if (rcode > 0) {
        if (strcmp(arg, "AUTHENTICATE") == 0
            && auth_data->authenticate_data != NULL) {
            if (verbose_flag)
                syslog(LOG_DEBUG, "About to parse %s",
                       auth_data->authenticate_data);
            rcode = parse_auth(auth_data->authenticate_data,
                               &auth_data->auth_struct);
            return rcode;
        } else
            return IDENT_INVALID_REQ_INFO;
    } else if (allow_trivial) {
        rcode = parse_auth(NULL, &auth_data->auth_struct);
        return rcode;
    } else
        return IDENT_UNKNOWN_ERROR;
}


/*
**  Send a response back to the requester.  Takes the response and the
**  auth_data struct (used to fill out the port details).  Always returns
**  true.
*/
int
do_out(char *string, struct ident_auth_data *auth_data)
{
    printf(" %d , %d : %s\r\n", auth_data->local_port,
           auth_data->remote_port, string);
    if (verbose_flag)
        syslog(LOG_DEBUG, " %d , %d : %s", auth_data->local_port,
               auth_data->remote_port, string);
    fflush(stdout);
    return 1;
}


/*
**  Return an error reply to the requester and log the error.  Takes the error
**  code and the auth_data struct (used to fill out the port details).  Does
**  not return; exit is called with the error code.
**
**  If the error code is above IDENT_INTERNAL_ERR, no reply is sent and the
**  connection is just immediately closed.
*/
int
do_error(int e_code, struct ident_auth_data *auth_data)
{
    char tmp[2048];
    int show_code;

    if (e_code < IDENT_INTERNAL_ERR) {
        show_code = e_code;
        if (unknown_flag)
            if (e_code == IDENT_NO_USER || e_code == IDENT_INVALID_PORT)
                show_code = IDENT_UNKNOWN_ERROR;
        sprintf(tmp, " ERROR : %s ", ident_err_txt[show_code]);
        do_out(tmp, auth_data);
        if (syslog_flag)
            syslog(LOG_INFO, "error: %s", ident_err_txt[e_code]);
    } else {
        switch (e_code) {
        case IDENT_NO_MUTUAL_AUTH:
            if (syslog_flag)
                syslog(LOG_INFO, "no mutual auth data");
            break;
        case IDENT_MUTUAL_AUTH_FAIL:
            if (syslog_flag)
                syslog(LOG_INFO, "mutual auth failed");
            break;
        case IDENT_SYSTEM_ERROR:
            if (syslog_flag)
                syslog(LOG_INFO, "system error: %m");
            break;
        default:
            if (syslog_flag)
                syslog(LOG_INFO, "unknown error code %d", e_code);
            break;
        }
    }
    exit(e_code);
}


/*
**  The core of the S/Ident daemon.  Parse the incoming request from the
**  requester and take appropriate action.  This is also where we do the
**  kernel query to flesh out the details of the request before passing them
**  off to the handlers for the different authentication methods.
**
**  Takes the FILE of the remote connection, the local Internet address, and
**  the remote Internet address (used in the kernel query).  Always returns 0;
**  on error, this function will never return.
*/
int
parse(FILE *fp, struct in_addr *laddr, struct in_addr *faddr)
{
    int try, rcode;
    int timeout = IDENT_READ_TIMEOUT;
    struct ident_auth_data auth_data;
    char line[2048];
    char *line_ptr;
    void *ka_ptr;
    struct kernel *kinfo_ptr;

    auth_data.local_addr = laddr;
    auth_data.remote_addr = faddr;
    auth_data.fp = fp;
    auth_data.local_port = 0;
    auth_data.remote_port = 0;

    /* Read query from client and parse it. */
    line_ptr = sfgets(line, 2048, fp, timeout, &auth_data);
    if (line_ptr == NULL)
        do_error(IDENT_SYSTEM_ERROR, &auth_data);
    rcode = parse_auth_line(line_ptr, &auth_data);
    if (rcode != IDENT_AUTH_OKAY)
        do_error(rcode, &auth_data);

    /* Validate the ports in the query. */
    if (!VALID_PORT(auth_data.local_port)
        || !VALID_PORT(auth_data.remote_port))
        do_error(IDENT_INVALID_PORT,&auth_data);
    if (client_only_flag)
        if (SERVER_PORT(auth_data.local_port))
            do_error(IDENT_NO_USER, &auth_data);

    /* Log the query. */
    if (syslog_flag)
        syslog(LOG_NOTICE, "from: %s (%s), for: %d, %d",
               inet_ntoa(*faddr), gethost(faddr), auth_data.local_port,
               auth_data.remote_port);

    /* Open the kernel and find the associated user. */
    if (verbose_flag)
        syslog(LOG_DEBUG, "After parsing, before k_open");
    if (ka_open(&ka_ptr) != 0) {
        if (syslog_flag)
            syslog(LOG_ERR, "ka_open call failed");
        do_error(IDENT_UNKNOWN_ERROR, &auth_data);
    }
    if (verbose_flag)
        syslog(LOG_DEBUG, "After ka_open, before ka_lookup");

    /* Get the specific TCP connection and return the uid.

       Try to fetch the information MAX_RETRY times in case the kernel changed
       beneath us and we missed or took a fault.  Used to be 5 times, but
       often this is not enough on Alpha OSF.

       If we failed in ka_lookup, that is presumably because the OS was busy
       creating or destroying processes.  We may want to sleep for a random
       time between retries, hoping for peace and quiet, but we don't
       currently. */
    kinfo_ptr = kernel_alloc();
    if (kinfo_ptr == NULL) {
        if (syslog_flag)
            syslog(LOG_ERR, "kernel_alloc call failed");
        do_error(IDENT_UNKNOWN_ERROR, &auth_data);
    }
    kinfo_ptr->local.sin_port = htons(auth_data.local_port);
    kinfo_ptr->remote.sin_port = htons(auth_data.remote_port);
    memcpy(&kinfo_ptr->local.sin_addr, auth_data.local_addr,
           sizeof(struct in_addr));
    memcpy(&kinfo_ptr->remote.sin_addr, auth_data.remote_addr,
           sizeof(struct in_addr));
    kinfo_ptr->local.sin_family = AF_INET ;
    kinfo_ptr->remote.sin_family = AF_INET ;
    kinfo_ptr->status = -1;

    /* Status will be set to 0 on not found, 1 on found, and -1 on failure. */
    for (try = 0; try < MAX_RETRY && kinfo_ptr->status < 0; try++)
        kinfo_ptr->status = ka_lookup(ka_ptr, kinfo_ptr);
    if (try >= MAX_RETRY) {
        if (syslog_flag)
            syslog(LOG_INFO,
                   "MAX_RETRY exceeded, returned: %d , %d : NO-USER",
                   auth_data.local_port, auth_data.remote_port);
        do_error(IDENT_NO_USER, &auth_data);
    }
    if (try > 1 && verbose_flag)
        syslog(LOG_DEBUG, "ka_lookup retries: %d", try);
    if (verbose_flag)
        syslog(LOG_DEBUG, "after ka_lookup, before getpwuid,"
               " kinfo_ptr status = %d", kinfo_ptr->status);
    if (kinfo_ptr->status == 0)
        do_error(IDENT_NO_USER,&auth_data);

    /* Try to use the real, not the effective, UID where possible. */
    if (kinfo_ptr->ruid == NO_UID)
        auth_data.uid = kinfo_ptr->euid;
    else
        auth_data.uid = kinfo_ptr->ruid;

    /* If we found a valid UID, change users.  We can do everything from this
       point forward as the user whose information is being looked up.  Check
       for the user's .nosident file at this point as well. */
    if (auth_data.uid > -1) {
        if (setuid(auth_data.uid) != 0) {
            syslog(LOG_ALERT, "setuid failed for uid %d", auth_data.uid);
            do_error(IDENT_UNKNOWN_ERROR, &auth_data);
        }
        if (check_nosident(auth_data.uid)) {
            syslog(LOG_DEBUG, "no ident requested by uid %d", auth_data.uid);
            do_error(IDENT_HIDDEN_USER, &auth_data);
        }
    } else {
        if (syslog_flag)
            syslog(LOG_INFO, "invalid uid %d", auth_data.uid);
        do_error(IDENT_UNKNOWN_ERROR, &auth_data);
    }

    /* We have a valid request.  Now do the real work. */
    if (syslog_flag)
        syslog(LOG_INFO, "AUTHENTICATE with %s",
               auth_data.auth_struct->auth_method);
    rcode = (*auth_data.auth_struct->start)(&auth_data);
    if (rcode != IDENT_AUTH_OKAY) {
        if (verbose_flag)
            syslog(LOG_DEBUG, "fail in auth_start");
        do_error(rcode, &auth_data);
    }
    rcode = (*auth_data.auth_struct->auth)(&auth_data);
    if (rcode != IDENT_AUTH_OKAY) {
        if (verbose_flag)
            syslog(LOG_DEBUG, "fail in auth_auth");
        do_error(rcode, &auth_data);
    }
    return 0;
}
