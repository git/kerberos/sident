/*  $Id: identd.h 545 2004-06-14 04:45:50Z eagle $
**
**  Common variables for the S/Ident daemon.
**
**  Written by Peter Eriksson <pen@lysator.liu.se>
**  S/Ident modifications by Booker C. Bense <bbense@stanford.edu>
** 
**  The pidentd portions of this program were originally under the following
**  license:  This program is in the public domain and may be used freely by
**  anyone who wants to.
*/

#ifndef IDENTD_H
#define IDENTD_H 1

#include <stdio.h> /* FILE */

#include "config.h" 
#include "sident.h" 

extern int verbose_flag;
extern int debug_flag;
extern int syslog_flag;
extern int unknown_flag;
extern int uidonly_flag; 
extern int users_can_nosident;
extern int allow_trivial; 
extern int mutual_auth_flag; 
extern int client_only_flag; 

extern char *gethost(struct in_addr *);
extern int parse(FILE *, struct in_addr *laddr, struct in_addr *faddr);

/* Port checking.  You may want to adjust SERVER_PORT for whatever makes sense
   at your site, in order to exclude other known server ports.  Note that
   these macros evaluate the port number multiple times. */
#define MAX_PORT        65535
#define VALID_PORT(p)   ((p) > 1 && (p) < MAX_PORT)
#define SERVER_PORT(p)  ((p) < 1024 || (p) == 6000)

#endif /* !IDENTD_H */
