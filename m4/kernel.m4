dnl kernel.m4 -- Find the appropriate kernel module.
dnl $Id: kernel.m4 567 2004-07-23 23:02:58Z eagle $
dnl
dnl Defines the macro SIDENT_KERNEL_MODULE, which probes for the appropriate
dnl kernel module to use on the local system, links the appropriate kernel
dnl module into the build tree, and warns if no appropriate kernel module
dnl could be found.
dnl
dnl Also does a variety of other tests and defines taken from the pidentd
dnl configure.in to set up things properly for what the kernel modules
dnl expect.
dnl
dnl Sets the substitution variable RESPONDER to "responder" if a valid kernel
dnl module is found and if building the responder is requested; otherwise, it
dnl is set to the empty string.

dnl Probe for the properties of struct nlist, used by k_bsd42.c.
AC_DEFUN([SIDENT_STRUCT_NLIST],
[AC_CHECK_HEADER([nlist.h],
    [AC_DEFINE([NLIST_STRUCT], 1, [Define to 1 if you have struct nlist.])
    AC_CACHE_CHECK([for n_un in struct nlist], [ac_cv_struct_nlist_n_un],
        [AC_TRY_COMPILE([#include <nlist.h>],
            [struct nlist n; n.n_un.n_name = 0;],
            [ac_cv_struct_nlist_n_un=yes], [ac_cv_struct_nlist_n_un=no])])
    if test $ac_cv_struct_nlist_n_un = yes; then
        AC_DEFINE([NLIST_NAME_UNION], 1,
            [Define to 1 if n_name in struct nlist is a union.])
    fi])])

dnl Check for the appropriate kernel module and make any other checks needed
dnl for that particular platform.  Set RESPONDER to the empty string if this
dnl host doesn't have a kernel module or if the user requested the responder
dnl not be built.
AC_DEFUN([SIDENT_KERNEL_MODULE],
[AC_REQUIRE([AC_CANONICAL_HOST])
RESPONDER=responder
AC_SUBST([RESPONDER])
AC_ARG_ENABLE([responder],
    AC_HELP_STRING([--disable-responder],
        [Do not build the sidentd responder]),
    [if test x"$enableval" = xno ; then
        RESPONDER=
     fi])
AC_MSG_CHECKING([for responder kernel module])

dnl Check for the appropriate kernel module and set a few other things
dnl depending on the host.
need_libkvm=yes
case "$host" in
*-aix4.3.2* | *-aix5*)
    kernel=k_aix432.c
    ;;
*-aix4.[123]*)
    kernel=k_aix42.c
    ;;
*-irix4*)
    kernel=k_irix4.c
    AC_DEFINE([_PATH_UNIX], "/unix", [Define to the path to the kernel.])
    ;;
*-irix5* | *-irix6*)
    kernel=k_irix5.c
    AC_DEFINE([_PATH_UNIX], "/unix", [Define to the path to the kernel.])
    ;;
*-hpux7*)
    kernel=k_bsd42.c
    AC_DEFINE([_PATH_UNIX], "/hp-ux", [Define to the path to the kernel.])
    if test "$ac_cv_prog_gcc" = "no"; then
        CPPFLAGS="$CPPFLAGS -DHPUX7 -Ae"
    fi
    ;;
*-hpux8* | *-hpux9*)
    kernel=k_bsd42.c
    AC_DEFINE([_PATH_UNIX], "/hp-ux", [Define to the path to the kernel.])
    if test "$ac_cv_prog_gcc" = "no"; then
        CPPFLAGS="$CPPFLAGS -Ae"
    fi
    ;;
*-hpux10*)
    kernel=k_bsd42.c
    AC_DEFINE([_PATH_UNIX], "/stand/vmunix",
        [Define to the path to the kernel.])
    if test "$ac_cv_prog_gcc" = "no"; then
        CPPFLAGS="$CPPFLAGS -Ae"
    fi
    ;;
*-linux*)
    kernel=k_linux.c
    need_libkvm=no
    ;;
*-nextstep3*)
    kernel=k_next_mach.c
    AC_DEFINE([_PATH_UNIX], "/mach", [Define to the path to the kernel.])
    ;;
*-ultrix4*)
    kernel=k_bsd42.c
    LIBS="$LIBS -li"
    ;;
*-osf4* | *-osf3.2*)
    kernel=k_osf4.c
    ;;
*-osf5*)
    kernel=k_osf5.c
    ;;
*-sunos4*)
    kernel=k_bsd42.c
    ;;
*-solaris2.4*)
    kernel=k_sunos54.c
    ;;
*-solaris2.5*)
    kernel=k_sunos55.c
    ;;
*-solaris2.6*)
    kernel=k_sunos56.c
    ;;
*-solaris2.7 | *-solaris2.8 | *-solaris2.9 | *-solaris2.10)
    case "$host" in
    *7)         kernel=k_sunos57.c ;;
    *8 | *9)    kernel=k_sunos58.c ;;
    *10)        kernel=k_sunos510.c ;;
    esac
    arch=`isainfo -k`
    if test x"$RESPONDER" = xresponder && test x"$arch" = xsparcv9 ; then
        if test "$ac_cv_prog_gcc" = "yes" ; then
            CFLAGS="$CFLAGS -m64"
        else
            CFLAGS="$CFLAGS -xarch=v9"
        fi
    fi
    ;;
*)
    kernel=k_dummy.c
    RESPONDER=
    ;;
esac
AC_MSG_RESULT($kernel)
if test "$kernel" = "k_dummy.c" && test x"$RESPONDER" = xresponder ; then
    AC_MSG_WARN([Could not locate a kernel file for $host])
    AC_MSG_WARN([Disabling building of the sidentd responder])
fi
AC_LINK_FILES([responder/kernel/$kernel], [responder/kernel.c])

dnl If we need kvm, check for it and check nlist properties.
if test "$need_libkvm" = "yes" ; then
    AC_DEFINE([NEED_LIBKVM], 1,
        [Define to 1 if this host uses kvm functions to find information.])
    SIDENT_STRUCT_NLIST
    AC_SEARCH_LIBS([nlist], [elf mld],
        [AC_DEFINE([HAVE_NLIST], 1,
            [Define to 1 if you have the nlist function.])])
    AC_SEARCH_LIBS([kvm_open], [kvm],
        [AC_DEFINE([HAVE_KVM_OPEN], 1,
            [Define to 1 if you have the kvm_open function.])])
fi])
