dnl krb5.m4 -- Find the compiler and linker flags for Kerberos v5.
dnl $Id: krb5.m4 2346 2006-01-02 00:27:26Z rra $
dnl
dnl Finds the compiler and linker flags and adds them to CPPFLAGS and LIBS.
dnl Provides --with-kerberos, --enable-reduced-depends, and --enable-static
dnl configure options to control how linking with Kerberos is done.  Uses
dnl krb5-config where available unless reduced dependencies is requested.
dnl Provides the macro SIDENT_LIB_KRB.

dnl Does the appropriate library checks for reduced-dependency GSSAPI linkage.
AC_DEFUN([_SIDENT_LIB_KRB_GSSAPI_REDUCED],
[AC_CHECK_LIB([gssapi], [gss_import_name],
    [KRBLIBS="-lgssapi"],
    [AC_CHECK_LIB([gssapi_krb5], [gss_import_name],
        [KRBLIBS="-lgssapi_krb5"],
        [AC_MSG_ERROR([cannot find usable GSSAPI library])])])])

dnl Does the appropriate library checks for reduced-dependency krb4 linkage.
AC_DEFUN([_SIDENT_LIB_KRB_KRB4_REDUCED],
[AC_CHECK_LIB([krb4], [krb_get_svc_in_tkt], [KRBLIBS="-lkrb4 $KRBLIBS"],
    [AC_CHECK_LIB([krb], [krb_get_svc_in_tkt], [KRBLIBS="-lkrb $KRBLIBS"],
        [AC_MSG_ERROR([cannot find usable Kerberos v4 library])])])])

dnl Does the appropriate library checks for GSSAPI linkage.
AC_DEFUN([_SIDENT_LIB_KRB_GSSAPI],
[AC_CHECK_LIB([gssapi], [gss_import_name],
    [KRBLIBS="-lgssapi -lkrb5 -lasn1 -lroken -lcrypto -lcom_err"],
    [KRB5EXTRA="-lkrb5 -lk5crypto -lcom_err"
     AC_CHECK_LIB([krb5support], [krb5int_getspecific],
        [KRB5EXTRA="$KRB5EXTRA -lkrb5support"],
        [AC_SEARCH_LIBS([pthread_setspecific], [pthreads pthread])
         AC_CHECK_LIB([krb5support], [krb5int_setspecific],
            [KRB5EXTRA="$KRB5EXTRA -lkrb5support"])])
     AC_CHECK_LIB([gssapi_krb5], [gss_import_name],
        [KRBLIBS="-lgssapi_krb5 $KRB5EXTRA"],
        [AC_MSG_ERROR([cannot find usable GSSAPI library])],
        [$KRB5EXTRA])],
    [-lkrb5 -lasn1 -lroken -lcrypto -lcom_err])])

dnl Does the appropriate library checks for krb4 linkage.
AC_DEFUN([_SIDENT_LIB_KRB_KRB4],
[KRB4EXTRA=
AC_CHECK_LIB([crypto], [des_set_key], [KRB4EXTRA="-lcrypto"],
    [KRB4EXTRA="-ldes"])
AC_CHECK_LIB([krb], [krb_get_svc_in_tkt],
    [KRBLIBS="-lkrb $KRB4EXTRA $KRBLIBS"],
    [KRB5EXTRA="-ldes425"
     AC_CHECK_LIB([krb4], [krb_get_svc_in_tkt],
        [KRBLIBS="-lkrb4 $KRB5EXTRA $KRBLIBS"],
        [AC_MSG_ERROR([cannot find usable Kerberos v4 library])],
        [$KRB5EXTRA $KRBLIBS])],
    [$KRB4EXTRA $KRBLIBS])])

dnl Additional checks for portability between MIT and Heimdal if GSSAPI
dnl libraries were requested.
AC_DEFUN([_SIDENT_LIB_KRB_GSSAPI_EXTRA],
[AC_CHECK_HEADERS([gssapi.h])
AC_CHECK_DECL([GSS_C_NT_USER_NAME],
    [AC_DEFINE([HAVE_GSS_RFC_OIDS], 1,
       [Define to 1 if the GSS-API library uses RFC-compliant OIDs.])], ,
[#ifdef HAVE_GSSAPI_H
# include <gssapi.h>
#else
# include <gssapi/gssapi.h>
#endif
])
AC_CHECK_DECLS([GSS_KRB5_MECHANISM], , ,
[#ifdef HAVE_GSSAPI_H
# include <gssapi.h>
#else
# include <gssapi/gssapi.h>
#endif
])])

dnl Additional checks for portability if krb4 libraries were requested.
AC_DEFUN([_SIDENT_LIB_KRB_KRB4_EXTRA],
[AC_CHECK_HEADERS([kerberosIV/krb.h])
AC_CHECK_FUNC([krb_life_to_time], , [AC_LIBOBJ([lifetime])])])

dnl The main macro.
AC_DEFUN([SIDENT_LIB_KRB],
[KRBROOT=
AC_ARG_WITH([kerberos],
    AC_HELP_STRING([--with-kerberos=DIR],
        [Location of Kerberos headers and libraries]),
    [if test x"$withval" != xno ; then
        KRBROOT="$withval"
     fi])

use_krb4=true
AC_ARG_ENABLE([krb4],
    AC_HELP_STRING([--disable-krb4], [Do not include Kerberos v4 support]),
    [if test x"$enableval" = xno ; then
        use_krb4=false
     fi])
if test x"$use_krb4" = xtrue ; then
    AC_DEFINE([HAVE_KRB4], [1], [Define if you want Kerberos v4 support.])
fi

reduce_depends=false
AC_ARG_ENABLE([reduced-depends],
    AC_HELP_STRING([--enable-reduced-depends],
        [Try to minimize shared library dependencies]),
    [if test x"$enableval" = xyes ; then
         if test x"$KRBROOT" != x ; then
             if test x"$KRBROOT" != x/usr ; then
                 CPPFLAGS="-I$KRBROOT/include"
             fi
             LDFLAGS="$LDFLAGS -L$KRBROOT/lib"
         fi
         _SIDENT_LIB_KRB_GSSAPI_REDUCED
         if test x"$use_krb4" = xtrue ; then
             _SIDENT_LIB_KRB_KRB4_REDUCED
         fi
         reduce_depends=true
     fi])

dnl Support static linkage as best we can.  Set a variable and do the
dnl wrapping later on.
static=false
AC_ARG_ENABLE([static-daemon],
    AC_HELP_STRING([--enable-static-daemon],
        [Link against the static Kerberos libraries]),
    [if test x"$enableval" = xyes ; then
         if test x"$reduce_depends" = xtrue ; then
             AC_MSG_ERROR([static daemon cannot be used with reduced depends])
         fi
         static=true
     fi])

dnl Checking for the neworking libraries shouldn't be necessary for the
dnl krb5-config case, but apparently it is at least for MIT Kerberos 1.2.
dnl This will unfortunately mean multiple -lsocket -lnsl references when
dnl building with current versions of Kerberos, but this shouldn't cause
dnl any practical problems.
if test x"$reduce_depends" != xtrue ; then
    AC_ARG_VAR([KRB5_CONFIG], [Path to krb5-config])
    if test x"$KRBROOT" != x ; then
        if test -x "$KRBROOT/bin/krb5-config" ; then
            KRB5_CONFIG="$KRBROOT/bin/krb5-config"
        fi
    else
        AC_PATH_PROG([KRB5_CONFIG], [krb5-config])
    fi

    # We can't use krb5-config if building static since we can't tell what
    # of the libraries it gives us should be static and which should be
    # dynamic.
    if test x"$KRB5_CONFIG" != x && test x"$static" != xtrue ; then
        AC_MSG_CHECKING([for gssapi support in krb5-config])
        if "$KRB5_CONFIG" | grep 'gssapi' > /dev/null 2>&1 ; then
            AC_MSG_RESULT([yes])
            KRBCPPFLAGS=`"$KRB5_CONFIG" --cflags gssapi`
            KRBLIBS=`"$KRB5_CONFIG" --libs gssapi`
        else
            AC_MSG_RESULT([no])
            KRBCPPFLAGS=`"$KRB5_CONFIG" --cflags`
            KRBLIBS=`"$KRB5_CONFIG" --libs`
        fi
        KRBCPPFLAGS=`echo "$KRBCPPFLAGS" | sed 's%-I/usr/include ?%%'`
        if test x"$use_krb4" = xtrue ; then
            _SIDENT_LIB_KRB_KRB4
        fi
    else
        if test x"$KRBROOT" != x ; then
            if test x"$KRBROOT" != x/usr ; then
                KRBCPPFLAGS="-I$KRBROOT/include"
            fi
            LDFLAGS="$LDFLAGS -L$KRBROOT/lib"
        fi
        AC_SEARCH_LIBS([res_search], [resolv], ,
            [AC_SEARCH_LIBS([__res_search], [resolv])])
        AC_SEARCH_LIBS([crypt], [crypt])
        _SIDENT_LIB_KRB_GSSAPI
        if test x"$use_krb4" = xtrue ; then
            _SIDENT_LIB_KRB_KRB4
        fi
    fi
    if test x"$KRBCPPFLAGS" != x ; then
        CPPFLAGS="$CPPFLAGS $KRBCPPFLAGS"
    fi
fi

dnl Generate the final library list and put it into the standard variables.
if test x"$static" = xtrue ; then
    PROG_LIBS="-Wl,-Bstatic $KRBLIBS -Wl,-Bdynamic $LIBS"
else
    PROG_LIBS="$KRBLIBS $LIBS"
fi
AC_SUBST([PROG_LIBS])
LIBS="$KRBLIBS $LIBS"
CPPFLAGS=`echo "$CPPFLAGS" | sed 's/^  *//'`
LDFLAGS=`echo "$LDFLAGS" | sed 's/^  *//'`
if test x"$reduce_depends" = xtrue ; then
    PERL_LIBS=
else
    PERL_LIBS="$LIBS"
fi
AC_SUBST([PERL_LIBS])

dnl Run any extra checks for the desired libraries.
_SIDENT_LIB_KRB_GSSAPI_EXTRA
if test x"$use_krb4" = xtrue ; then
    _SIDENT_LIB_KRB_KRB4_EXTRA
fi])
