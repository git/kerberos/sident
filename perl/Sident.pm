package Net::Sident;

use strict;
use Carp;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK $AUTOLOAD);

require Exporter;
require DynaLoader;
require AutoLoader;

@ISA = qw(Exporter DynaLoader);
# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT = qw(
	IDENT_AUTH_FAIL
	IDENT_AUTH_NOT_SUPPORTED
	IDENT_AUTH_OKAY
	IDENT_FLAG_NOT_SUPPORTED
	IDENT_HIDDEN_USER
	IDENT_INTERNAL_ERR
	IDENT_INVALID_FLAG_VALUE
	IDENT_INVALID_PORT
	IDENT_INVALID_REQ_INFO
	IDENT_INVALID_RESP_INFO
	IDENT_MAX_ERROR
	IDENT_MUTUAL_AUTH_FAIL
	IDENT_NO_MUTUAL_AUTH
	IDENT_NO_USER
	IDENT_READ_TIMEOUT
	IDENT_SYSTEM_ERROR
	IDENT_TIMEOUT
	IDENT_UNKNOWN_ERROR
	IDENT_USER_CANT_AUTH
	IDENT_USER_WONT_AUTH
	set_authtype
	get_authflag
	set_authflag
	lookup
	query
);
$VERSION = '0.2';

sub AUTOLOAD {
    # This AUTOLOAD is used to 'autoload' constants from the constant()
    # XS function.  If a constant is not found then control is passed
    # to the AUTOLOAD in AutoLoader.

    my $constname;
    ($constname = $AUTOLOAD) =~ s/.*:://;
    croak "& not defined" if $constname eq 'constant';
    my $val = constant($constname, @_ ? $_[0] : 0);
    if ($! != 0) {
	if ($! =~ /Invalid/) {
	    $AutoLoader::AUTOLOAD = $AUTOLOAD;
	    goto &AutoLoader::AUTOLOAD;
	}
	else {
		croak "Your vendor has not defined Net::Sident macro $constname";
	}
    }
    eval " sub $AUTOLOAD { $val } ";
    goto &$AUTOLOAD;
}

bootstrap Net::Sident $VERSION;

# Preloaded methods go here.

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is the stub of documentation for your module. You better edit it!

=head1 NAME

Net::Sident - Perl extension for Secure Ident Protocol

=head1 SYNOPSIS

    use Net::Sident qw(set_authtype set_authflag lookup query);
    $status = set_authtype ("KERBEROS_V4", "/etc/leland/ident.srvtab");
    $status = set_authflag ("USER-INTERACTION", "YES");

    ($requester_port, $responder_port, $identifier, $opsys, $charset,
     $status) =	lookup (fileno (STDIN), $timeout);
    ($requester_port, $responder_port, $identifier, $opsys, $charset,
     $status) = query ($responder_ip, $responder_port, $requester_ip,
                       $requester_port, $timeout);

=head1 DESCRIPTION

Net::Sident is a simple functional interface to the C libsident library.
The set_authtype call should be used before any attempts to use any other
part of the library.

There are two lookup methods:  lookup() taks a file number and timeout as
arguements, and query takes an IP address (as a string) and port number of 
both the requester and responder, plus the timeout parameter.  If you have
an open file descriptor to the remote client, lookup() is the simplest
interface; query() is only needed in more complicated cases.

Here's an an example.  This asks for information about the client connecting
to port 25 on localhost, where the client's port is 8000.  A 30 second
timeout is set:

    query ("127.0.0.1", 25, "127.0.0.1", 8000, 30);

If the connection from the client is on standard input of the Perl script
(typical if, for instance, the Perl script is being run out of B<inetd>),
the following is equivalent and much simpler:

    lookup (fileno (STDIN), 30);

Additional flags can be set or retrieved with set_authflag() and
get_authflag().  The status returned by Net::Sident will be one of the
following constants, all of which are expored by the Net::Sident module:

    IDENT_AUTH_FAIL
    IDENT_AUTH_NOT_SUPPORTED
    IDENT_AUTH_OKAY
    IDENT_BUFFER_SIZE
    IDENT_FLAG_NOT_SUPPORTED
    IDENT_HIDDEN_USER
    IDENT_INTERNAL_ERR
    IDENT_INVALID_FLAG_VALUE
    IDENT_INVALID_PORT
    IDENT_INVALID_REQ_INFO
    IDENT_INVALID_RESP_INFO
    IDENT_MAX_ERROR
    IDENT_MUTUAL_AUTH_FAIL
    IDENT_NO_MUTUAL_AUTH
    IDENT_NO_USER
    IDENT_READ_TIMEOUT
    IDENT_SYSTEM_ERROR
    IDENT_TIMEOUT
    IDENT_UNKNOWN_ERROR
    IDENT_USER_CANT_AUTH
    IDENT_USER_WONT_AUTH

For more information on the meaning of these constants, see sident(3).  The
returned identifier will normally be the Kerberos identity of the remote
user, a colon, and then the local Unix username of the remote user.  Note
that the latter is rarely particularly useful.

=head1 WARNING

The S/Ident protocol implemented by this package is inherently vulnerable
to an active man-in-the-middle attack.  If an attacker can interpose
themselves into a network connection initiated by a victim and both
impersonate that victim and selectively control which of their packets
reach a server using S/Ident, the attacker can make use of the victim's
authentication credentials.  The attacker cannot initiate the session,
only hijack an existing authenticated session.

Because of this, you should very carefully analyze the security
requirements of any service for which you're considering deploying S/Ident
authentication.  Due to the requirements of the attack, S/Ident may still
be appropriate for very light authentication or in secure network
environments, but should not be used for general authentication on
untrusted networks.

=head1 SEE ALSO

sident(3), sidentd(8)

=head1 AUTHORS

Originally written by Booker C. Bense <bbense@stanford.edu>.  Currently
maintained by Russ Allbery <rra@stanford.edu>.

=cut
