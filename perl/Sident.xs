#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

 
#include "patchlevel.h"
#if PATCHLEVEL < 5 && SUBVERSION < 5
   #define PL_sv_undef sv_undef
#endif

#if PATCHLEVEL < 6
   #define BROKEN_XS
#endif 

#include <sident.h>

static int
not_here(char *s)
{
    croak("%s not implemented on this architecture", s);
    return -1;
}

static double
constant(char *name, int arg)
{
    errno = 0;
    switch (*name) {
    case 'I':
	if (strEQ(name, "IDENT_AUTH_FAIL"))
	    return IDENT_AUTH_FAIL;
	if (strEQ(name, "IDENT_AUTH_NOT_SUPPORTED"))
	    return IDENT_AUTH_NOT_SUPPORTED;
	if (strEQ(name, "IDENT_AUTH_OKAY"))
	    return IDENT_AUTH_OKAY;
	if (strEQ(name, "IDENT_FLAG_NOT_SUPPORTED"))
	    return IDENT_FLAG_NOT_SUPPORTED;
	if (strEQ(name, "IDENT_HIDDEN_USER"))
	    return IDENT_HIDDEN_USER;
	if (strEQ(name, "IDENT_INTERNAL_ERR"))
	    return IDENT_INTERNAL_ERR;
	if (strEQ(name, "IDENT_INVALID_FLAG_VALUE"))
	    return IDENT_INVALID_FLAG_VALUE;
	if (strEQ(name, "IDENT_INVALID_PORT"))
	    return IDENT_INVALID_PORT;
	if (strEQ(name, "IDENT_INVALID_REQ_INFO"))
	    return IDENT_INVALID_REQ_INFO;
	if (strEQ(name, "IDENT_INVALID_RESP_INFO"))
	    return IDENT_INVALID_RESP_INFO;
	if (strEQ(name, "IDENT_MAX_ERROR"))
	    return IDENT_MAX_ERROR;
	if (strEQ(name, "IDENT_MUTUAL_AUTH_FAIL"))
	    return IDENT_MUTUAL_AUTH_FAIL;
	if (strEQ(name, "IDENT_NO_MUTUAL_AUTH"))
	    return IDENT_NO_MUTUAL_AUTH;
	if (strEQ(name, "IDENT_NO_USER"))
	    return IDENT_NO_USER;
	if (strEQ(name, "IDENT_READ_TIMEOUT"))
	    return IDENT_READ_TIMEOUT;
	if (strEQ(name, "IDENT_SYSTEM_ERROR"))
	    return IDENT_SYSTEM_ERROR;
	if (strEQ(name, "IDENT_TIMEOUT"))
	    return IDENT_TIMEOUT;
	if (strEQ(name, "IDENT_UNKNOWN_ERROR"))
	    return IDENT_UNKNOWN_ERROR;
	if (strEQ(name, "IDENT_USER_CANT_AUTH"))
	    return IDENT_USER_CANT_AUTH;
	if (strEQ(name, "IDENT_USER_WONT_AUTH"))
	    return IDENT_USER_WONT_AUTH;
	break;
    }
    errno = EINVAL;
    return 0;
}



MODULE = Net::Sident		PACKAGE = Net::Sident

PROTOTYPES: DISABLE

double
constant(name,arg)
	char *		name
	int		arg

int
set_authtype(type,data)
	char *		type
	char *		data
	CODE: 
	RETVAL = ident_set_authtype(type,data) ; 
	OUTPUT:
	RETVAL


int 
set_authflag(flag,value)
	char *		flag
	char *		value
	CODE: 
	RETVAL = ident_set_authflag( flag, value) ; 
	OUTPUT:
	RETVAL		

char *
get_authflag(flag) 
	char *		flag

	PREINIT:
 	char *		value = NULL ;
	int 		status  ; 
	CODE: 
	status = ident_get_authflag(flag,&value) ;
	RETVAL = value ; 	
	OUTPUT:
	RETVAL

void
lookup ( socket, timeout) 
	int 	socket
	int 	timeout
	PREINIT: 
	IDENT *ident ; 
	PPCODE: 
	ident = ident_lookup(socket,timeout) ;
	if ( ident == NULL ) { 
#ifndef BROKEN_XS
		/* perl < 5.6.0 barfs on this */ 
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ; 
	        /* Need put something in status */
#else
		XPUSHs(sv_2mortal(newSViv(0))) ;	
	        XPUSHs(sv_2mortal(newSViv(0))) ;	
		XPUSHs(sv_2mortal(newSVpv(" ",1))) ;	
		XPUSHs(sv_2mortal(newSVpv(" ",1))) ;	
		XPUSHs(sv_2mortal(newSVpv(" ",1))) ;	
	        XPUSHs(sv_2mortal(newSViv(IDENT_UNKNOWN_ERROR))) ;	
#endif 
	} else {  
		XPUSHs(sv_2mortal(newSViv(ident->resp_port))); 
		XPUSHs(sv_2mortal(newSViv(ident->req_port))); 
		if ( ident->identifier != NULL ) { 
			XPUSHs(sv_2mortal(newSVpv(ident->identifier,strlen(ident->identifier)))); 
		} else {
	 		XPUSHs(&PL_sv_undef) ; 
		}
		if ( ident->opsys != NULL )  { 
			XPUSHs(sv_2mortal(newSVpv(ident->opsys,strlen(ident->opsys)))); 
		} else {
                	XPUSHs(&PL_sv_undef) ;
        	}
		if ( ident->charset != NULL ) {
			XPUSHs(sv_2mortal(newSVpv(ident->charset,strlen(ident->charset))));  
		} else {
                	XPUSHs(&PL_sv_undef) ;
        	}

		XPUSHs(sv_2mortal(newSViv(ident->result_code))); 
		ident_free(ident) ;
	} 
	XSRETURN(6) ; 

void
query (resp_addr,resp_port,req_addr,req_port, timeout) 
	char	* resp_addr
	int	resp_port
	char	* req_addr
	int	req_port
	int 	timeout
	PREINIT: 
	IDENT *ident ; 
	struct in_addr responder ; 
	struct in_addr requester ; 

	PPCODE: 
	
	responder.s_addr = inet_addr(resp_addr) ; 
	requester.s_addr = inet_addr(req_addr) ; 	

	ident = ident_query(&requester,&responder,resp_port,req_port,timeout) ; 	if ( ident == NULL ) { 
#ifndef BROKEN_XS
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	
		XPUSHs(&PL_sv_undef) ;	/* Need to put something in status */
#else
		XPUSHs(sv_2mortal(newSViv(0))) ;	
	        XPUSHs(sv_2mortal(newSViv(0))) ;	
		XPUSHs(sv_2mortal(newSVpv(" ",1))) ;	
		XPUSHs(sv_2mortal(newSVpv(" ",1))) ;	
		XPUSHs(sv_2mortal(newSVpv(" ",1))) ;	
	        XPUSHs(sv_2mortal(newSViv(IDENT_UNKNOWN_ERROR))) ;	 
#endif 
	} else { 
		XPUSHs(sv_2mortal(newSViv(ident->resp_port))); 
		XPUSHs(sv_2mortal(newSViv(ident->req_port))); 
		if ( ident->identifier != NULL ) { 
			XPUSHs(sv_2mortal(newSVpv(ident->identifier,strlen(ident->identifier)))); 
		} else {
	 		XPUSHs(&PL_sv_undef) ; 
		}
		if ( ident->opsys != NULL )  { 
			XPUSHs(sv_2mortal(newSVpv(ident->opsys,strlen(ident->opsys)))); 
		} else {
                	XPUSHs(&PL_sv_undef) ;
        	}
		if ( ident->charset != NULL ) {
			XPUSHs(sv_2mortal(newSVpv(ident->charset,strlen(ident->charset))));  
		} else {
                	XPUSHs(&PL_sv_undef) ;
        	}

		XPUSHs(sv_2mortal(newSViv(ident->result_code))); 
		ident_free(ident) ; 
	}
	XSRETURN(6) ; 
