# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

BEGIN { $| = 1; print "1..3\n"; }
END {print "not ok 1\n" unless $loaded;}
use Net::Sident;
$loaded = 1;
print "ok 1\n";

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

use vars qw( $status $set_value $value ) ;
# Test 2
$status = 1 ; 
$value = "NO" ; 

$status = set_authtype("KERBEROS_V4","/etc/leland/ident.srvtab") ; 

print "status = $status\n" ; 

if ( $status ) {
print "not ok 2\n" ; 	
} else { 
print "ok 2\n" ; 
}

# Test 3
$set_value = "YES" ; 
$status = set_authflag("USER-INTERACTION",$set_value) ; 

print "status = $status\n" ; 

if ( $status ) {
print "not ok 3\n" ;
} else {
print "ok 3\n" ;
}

# Test 4
# Get_authflag is broken in sident library... 
#
#$value = get_authflag("USER-INTERACTION") ; 
#
#print "$status, $value , $set_value\n" ; 
#
#if ( $value eq "YES" && $status == 0 ) { 
#  print "ok 4\n" ; 
#} else { 
#  print "not ok 4\n" ; 
#} 
