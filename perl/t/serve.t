# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

BEGIN { $| = 1; print "1..4\n"; }
END {print "not ok 1\n" unless $loaded;}
use Net::Sident;
$loaded = 1;
print "ok 1\n";

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

use vars qw( $status ) ;
# Test 2
$status = 1 ; 

# Note: This file must contain the ident.host srvtab. You 
# will likely have to be root to read this file. 

$status = set_authtype("KERBEROS_V4","/etc/leland/srvtab.ident") ; 

print "status = $status\n" ; 

if ( $status ) {
print "not ok 2\n" ; 	
} else { 
print "ok 2\n" ; 
}

# We need to fork a client/server here, yick.... 
# This test assumes that you have an sident responder 
# running on your machine. Also you will most likely 
# have to be root to run it successfully. 
#
srand( $$ ) ; 
use vars qw( $i $host $port $pid $client $address
 $resp_port $req_port $id $opsys $charset $status $timeout) ; 
 
$port = int (9000 + rand(2000) );               # change this at will

$| = 1 ; 
use POSIX;
use IO::Socket;
use IO::Select;
use Socket;
use Fcntl;
use Sys::Hostname ; 

if ( $pid = fork ) { 
  # Parent 
  # sleep for a while
  sleep(5) ; 
  print "Connecting to $port from parent.\n" ; 
  # connect to server
  $host = hostname() ;
  $address = gethostbyname($host) ; 
  $host = gethostbyaddr($address,AF_INET) ; 
  unless ( $host) { die "Cannot find my own name." ; }  
  
  $client = IO::Socket::INET->new(PeerAddr => $host, 
				  PeerPort => $port,
				  Proto    => "tcp",
				  Type     => SOCK_STREAM
				  ) ; 
  while ( <$client> ) { 
    print $_ ; 
    $id = chomp($_) ; 
  } 
  close( $client) ; 

  
  
  if ( defined $id ) { 
    print "ok 3\n" ; 
  } else { 
    print "not ok 3\n"; 
  }
# Now test query  
  $client = IO::Socket::INET->new(PeerAddr => $host, 
				  PeerPort => $port,
				  Proto    => "tcp",
				  Type     => SOCK_STREAM
				  ) ; 
  while ( <$client> ) { 
    print $_ ; 
    $id = chomp($_) ; 
  } 
  close( $client) ; 

  
  if ( defined $id ) { 
    print "ok 4\n" ; 
  } else { 
    print "not ok 4\n"; 
  }

  # Kill child 
  kill 9 , $pid ; 

} else { 
  die "cannot fork : $!" unless defined $pid ; 
  # Child 
  # Set up test server
  print "Starting server on $port in child.\n" ; 
  # Listen to port.
  $server = IO::Socket::INET->new(LocalPort => $port,
				  Type      => SOCK_STREAM,
				  Reuse     => 1, 
				  Listen    => 10 )
    or die "Can't make server socket: $@\n";
  $server->autoflush() ; 
  
  
  while ( $connection = $server->accept() ) { 
  
    $timeout = 30 ; 

    if ( ($i++)%2 == 0 ) { 
      print "Testing lookup\n" ; 

      ($resp_port,$req_port,$id,$opsys,$charset,$status ) = 
	lookup($connection->fileno(),$timeout) ; 

    } else { 
      print "Testing query\n" ;  
      $my_ip = $connection->sockhost() ; 
      $my_port = $connection->sockport() ; 
      $their_ip = $connection->peerhost() ; 
      $their_port = $connection->peerport() ; 
      

      ($resp_port,$req_port,$id,$opsys,$charset,$status ) = 
        query($their_ip,$their_port,$my_ip,$my_port,$timeout) ; 

    }
    $opsys ||= '';
    $charset ||= '';

    print "Responder port =$resp_port\n" ; 
    print "Requester port =$req_port\n" ; 
    print "Id =$id\n" ; 
    print "Opsys =$opsys\n" ; 
    print "Charset =$charset\n" ; 
    print "Status code =$status\n" ; 
    
    $connection->print( "You are $id.\n") ; 
    # Close the connection
    $connection->close() ; 
  } 

}

